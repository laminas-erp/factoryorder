<?php

namespace Lerp\Factoryorder\Form;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Filter\DateTimeFormatter;
use Laminas\Filter\StringTrim;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Digits;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class FactoryorderForm extends AbstractForm implements InputFilterProviderInterface
{
    protected array $locationPlaceUuidAssoc = [];

    public function setLocationPlaceUuidAssoc(array $locationPlaceUuidAssoc): void
    {
        $this->locationPlaceUuidAssoc = $locationPlaceUuidAssoc;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'factoryorder_uuid']);
        }
        $this->add(['name' => 'factoryorder_briefing']);
        $this->add(['name' => 'factoryorder_quantity']);
        $this->add(['name' => 'factoryorder_time_finish_schedule']);
        $this->add(['name' => 'location_place_uuid']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['factoryorder_uuid'] = [
                'required' => true,
                'filters' => [
                    ['name' => FilterChainStringSanitize::class]
                ], 'validators' => [
                    ['name' => Uuid::class]
                ]
            ];
        }

        $filter['factoryorder_briefing'] = [
            'required' => false,
            'filters' => [
                ['name' => FilterChainStringSanitize::class]
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 62000,
                    ]
                ]
            ]
        ];

        $filter['factoryorder_quantity'] = [
            'required' => true,
            'filters' => [
                ['name' => FilterChainStringSanitize::class]
            ], 'validators' => [
                [
                    'name' => Digits::class,
                ]
            ]
        ];

        $filter['factoryorder_time_finish_schedule'] = [
            'required' => false,
            'filters' => [
                [
                    'name' => DateTimeFormatter::class,
//                    'options' => [
//                        'format' => 'Y-m-d H:i:s'
//                    ]
                ]
            ], 'validators' => [
//                [
//                    'name' => Date::class,
//                    'options' => [
//                        'format' => 'Y-m-d H:i:s.v'
//                    ]
//                ]
            ]
        ];

        $filter['location_place_uuid'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class]
            ],
            'validators' => [
                [
                    'name' => InArray::class,
                    'options' => [
                        'haystack' => array_keys($this->locationPlaceUuidAssoc)
                    ]
                ]
            ]
        ];

        return $filter;
    }
}

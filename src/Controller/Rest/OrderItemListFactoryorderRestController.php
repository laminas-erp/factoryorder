<?php

namespace Lerp\Factoryorder\Controller\Rest;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Factoryorder\Service\FactoryorderService;

class OrderItemListFactoryorderRestController extends AbstractUserRestController
{
    protected FactoryorderService $factoryorderService;

    public function setFactoryorderService(FactoryorderService $factoryorderService): void
    {
        $this->factoryorderService = $factoryorderService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $p = $this->getRequest()->getPost()->toArray();
        if (empty($p)) {
            return $jsonModel;
        }
        $oilUuidQntty = [];
        $uuid = new Uuid();
        foreach ($p as $oilUuid => $qntty) {
            if (!$uuid->isValid($oilUuid) || empty(floatval($qntty))) {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
                return $jsonModel;
            }
            $oilUuidQntty[$oilUuid] = floatval($qntty);
        }
        if ($this->factoryorderService->createFactoryordersForOrderItemLists($oilUuidQntty, $this->userService->getUserUuid())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}

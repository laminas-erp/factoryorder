<?php

namespace Lerp\Factoryorder\Controller\Rest;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Factoryorder\Entity\ParamsFactoryorderEntity;
use Lerp\Factoryorder\Form\FactoryorderForm;
use Lerp\Factoryorder\Service\FactoryorderService;

class FactoryorderRestController extends AbstractUserRestController
{
    protected FactoryorderService $factoryorderService;
    protected FactoryorderForm $factoryorderForm;

    public function setFactoryorderService(FactoryorderService $factoryorderService): void
    {
        $this->factoryorderService = $factoryorderService;
    }

    public function setFactoryorderForm(FactoryorderForm $factoryorderForm): void
    {
        $this->factoryorderForm = $factoryorderForm;
    }

    /**
     * GET Search factoryorders
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $qp = new ParamsFactoryorderEntity();
        $qp->setFromParamsArray($this->params()->fromQuery());
        if (!empty($arr = $this->factoryorderService->searchFactoryorders($qp))) {
            $jsonModel->setArr($arr);
            $jsonModel->setCount($this->factoryorderService->getSearchFactoryorderCount());
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($obj = $this->factoryorderService->getFactoryorder($id))) {
            $jsonModel->setObj($obj);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id factoryorder_uuid
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $data['factoryorder_uuid'] = $id;
        $this->factoryorderForm->setPrimaryKeyAvailable(true);
        $this->factoryorderForm->init();
        $this->factoryorderForm->setData($data);
        if (!$this->factoryorderForm->isValid()) {
            $jsonModel->addMessages($this->factoryorderForm->getMessages());
            return $jsonModel;
        }
        if (!$this->factoryorderService->updateFactoryorder($this->factoryorderForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}

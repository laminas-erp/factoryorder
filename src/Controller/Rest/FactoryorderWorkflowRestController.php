<?php

namespace Lerp\Factoryorder\Controller\Rest;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Document\Service\FactoryorderDocumentService;
use Lerp\Factoryorder\Form\FactoryorderWorkflowForm;
use Lerp\Factoryorder\Service\FactoryorderService;
use Lerp\Factoryorder\Service\FactoryorderWorkflowService;

class FactoryorderWorkflowRestController extends AbstractUserRestController
{
    protected FactoryorderService $factoryorderService;
    protected FactoryorderWorkflowService $factoryorderWorkflowService;
    protected FactoryorderWorkflowForm $factoryorderWorkflowForm;
    protected FactoryorderDocumentService $factoryorderDocumentService;

    public function setFactoryorderService(FactoryorderService $factoryorderService): void
    {
        $this->factoryorderService = $factoryorderService;
    }

    public function setFactoryorderWorkflowService(FactoryorderWorkflowService $factoryorderWorkflowService): void
    {
        $this->factoryorderWorkflowService = $factoryorderWorkflowService;
    }

    public function setFactoryorderWorkflowForm(FactoryorderWorkflowForm $factoryorderWorkflowForm): void
    {
        $this->factoryorderWorkflowForm = $factoryorderWorkflowForm;
    }

    public function setFactoryorderDocumentService(FactoryorderDocumentService $factoryorderDocumentService): void
    {
        $this->factoryorderDocumentService = $factoryorderDocumentService;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        if (!(new Uuid())->isValid(($foUuid = $this->params()->fromQuery('fo_uuid')))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->factoryorderWorkflowService->getFactoryorderWorkflows($foUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        $this->factoryorderWorkflowForm->setData($data);
        if (!$this->factoryorderWorkflowForm->isValid()) {
            $jsonModel->addMessages($this->factoryorderWorkflowForm->getMessages());
            return $jsonModel;
        }
        $formData = $this->factoryorderWorkflowForm->getData();
        if (!empty($foWorkflowUuid = $this->factoryorderWorkflowService->insertFactoryorderWorkflow(
            $formData['factoryorder_uuid'],
            $formData['workflow_uuid'],
            $formData['factoryorder_workflow_price_per_hour']
        )
        )) {
            $jsonModel->setVariable('fo_workflow_uuid', $foWorkflowUuid);
            $jsonModel->setSuccess(1);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }

        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id factoryorder_workflow_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->factoryorderDocumentService->existDocForFactoryorderWorkflow($id)) {
            $jsonModel->addMessage('Can not delete workflow: Document exist for this workflow.');
            return $jsonModel;
        }
        if ($this->factoryorderWorkflowService->deleteFactoryorderWorkflow($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id factoryorder_workflow_uuid
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($data['move']) && !empty($data['direction'])) {
            if ($this->factoryorderWorkflowService->updateFactoryorderWorkflowOrderPriorityMove($id, $data['direction'])) {
                $jsonModel->setSuccess(1);
            }
        } else {
            $data['factoryorder_workflow_uuid'] = $id;
            $this->factoryorderWorkflowForm->setPrimaryKeyAvailable(true);
            $this->factoryorderWorkflowForm->setData($data);
            if (!$this->factoryorderWorkflowForm->isValid()) {
                $jsonModel->addMessages($this->factoryorderWorkflowForm->getMessages());
                return $jsonModel;
            }
            $formData = $this->factoryorderWorkflowForm->getData();
            if ($this->factoryorderWorkflowService->updateFactoryorderWorkflow(
                $formData['factoryorder_workflow_uuid'],
                $formData['workflow_uuid'],
                $formData['factoryorder_workflow_time'],
                $formData['factoryorder_workflow_price_per_hour'],
                $formData['factoryorder_workflow_order_priority'],
                $formData['factoryorder_workflow_text'] ?? ''
            )
            ) {
                $jsonModel->setSuccess(1);
            }
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}

<?php

namespace Lerp\Factoryorder\Controller\Rest;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Factoryorder\Service\FactoryorderService;

class OrderItemFactoryorderRestController extends AbstractUserRestController
{
    protected FactoryorderService $factoryorderService;

    public function setFactoryorderService(FactoryorderService $factoryorderService): void
    {
        $this->factoryorderService = $factoryorderService;
    }

    /**
     * Create a factory order from an order item.
     * POST maps to create().
     * @param array $data ['order_item_uuid' => '', 'do_order_item_list' => true|false]
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (
            !isset($data['order_item_uuid']) || !(new Uuid())->isValid($data['order_item_uuid'])
            || (isset($data['do_order_item_list']) && !in_array($data['do_order_item_list'], ['true', 'false']))
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->factoryorderService->createFactoryordersForOrderItem($data['order_item_uuid'], $this->userService->getUserUuid(), $data['do_order_item_list'])) {
            $jsonModel->setSuccess(1);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        } else {
            $jsonModel->addMessage($this->factoryorderService->getMessage());
        }
        return $jsonModel;
    }

    /**
     * GET factory orders for order otem from db.view_factoryorder.
     * @param string $id order_item_uuid
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($fos = $this->factoryorderService->getFactoryordersForOrderItem($id))) {
            $jsonModel->setSuccess(1);
            $jsonModel->setArr($fos);
        }
        return $jsonModel;
    }
}

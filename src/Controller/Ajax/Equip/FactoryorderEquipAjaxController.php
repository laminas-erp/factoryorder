<?php

namespace Lerp\Factoryorder\Controller\Ajax\Equip;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Factoryorder\Entity\ParamsFactoryorderWorkflowEquipEntity;
use Lerp\Factoryorder\Service\FactoryorderEquipService;

class FactoryorderEquipAjaxController extends AbstractUserController
{
    protected FactoryorderEquipService $factoryorderEquipService;

    public function setFactoryorderEquipService(FactoryorderEquipService $factoryorderEquipService): void
    {
        $this->factoryorderEquipService = $factoryorderEquipService;
    }

    /**
     * POST
     * Factoryorders from db.view_factoryorder_workflow_equip
     * @return JsonModel
     */
    public function factoryorderWorkflowsEquipAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $params = new ParamsFactoryorderWorkflowEquipEntity();
        $params->setFromParamsArray($this->getRequest()->getPost()->toArray());
        if (!$params->isSuccess()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            $jsonModel->addMessages($params->getMessages());
            return $jsonModel;
        }
        $jsonModel->setArr($this->factoryorderEquipService->searchFactoryorderWorkflowsEquip($params));
        $jsonModel->setCount($this->factoryorderEquipService->getSearchFactoryorderCount());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function linkFactoryorderToEquipAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $foUuid = $this->params('fo_uuid');
        $equipUuid = $this->params('equip_uuid');
        $uuid = new Uuid();
        if (!$uuid->isValid($foUuid) || !$uuid->isValid($equipUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if($this->factoryorderEquipService->updateFactoryorderEquipment($foUuid, $equipUuid)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}

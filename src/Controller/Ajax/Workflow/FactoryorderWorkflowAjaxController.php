<?php

namespace Lerp\Factoryorder\Controller\Ajax\Workflow;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Factoryorder\Service\FactoryorderService;
use Lerp\Factoryorder\Service\FactoryorderWorkflowService;

class FactoryorderWorkflowAjaxController extends AbstractUserController
{
    protected FactoryorderWorkflowService $factoryorderWorkflowService;
    protected FactoryorderService $factoryorderService;

    public function setFactoryorderWorkflowService(FactoryorderWorkflowService $factoryorderWorkflowService): void
    {
        $this->factoryorderWorkflowService = $factoryorderWorkflowService;
    }

    public function setFactoryorderService(FactoryorderService $factoryorderService): void
    {
        $this->factoryorderService = $factoryorderService;
    }

    /**
     * @return JsonModel
     */
    public function updateFactoryorderBriefingAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($uuid = $this->params('fo_uuid'))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $briefing = filter_input(INPUT_POST, 'fo_briefing', FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if ($this->factoryorderService->updateFactoryorderBriefing($uuid, $briefing)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function updateFactoryorderWorkflowOrderPriorityAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5) || !$this->getRequest()->isPost()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $foWorkflowUuid = $this->params('fowf_uuid');
        $orderPrioPrevious = filter_input(INPUT_POST, 'order_prio_previous', FILTER_SANITIZE_NUMBER_INT);
        $orderPrioCurrent = filter_input(INPUT_POST, 'order_prio_current', FILTER_SANITIZE_NUMBER_INT);
        if (!(new Uuid())->isValid($foWorkflowUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->factoryorderWorkflowService->updateFactoryorderWorkflowOrderPriority($foWorkflowUuid, $orderPrioPrevious, $orderPrioCurrent)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}

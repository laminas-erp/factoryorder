<?php

namespace Lerp\Factoryorder\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Factoryorder\Entity\FactoryorderEntity;
use Lerp\Factoryorder\Service\FactoryorderService;
use Lerp\OperatingLicense\Entity\ViewOperatingLicenseItemEntity;
use Lerp\OperatingLicense\Form\ViewOperatingLicenseItemForm;
use Lerp\OperatingLicense\Service\OperatingLicenseService;
use Lerp\Product\Service\ProductService;
use Lerp\Stock\Entity\StockinEntity;
use Lerp\Stock\Form\StockinForm;
use Lerp\Stock\Service\StockService;

class FactoryorderAjaxController extends AbstractUserController
{
    protected FactoryorderService $factoryorderService;
    protected ProductService $productService;
    protected StockinForm $stockinForm;
    protected StockService $stockService;
    protected ViewOperatingLicenseItemForm $viewOperatingLicenseItemForm;

    public function setFactoryorderService(FactoryorderService $factoryorderService): void
    {
        $this->factoryorderService = $factoryorderService;
    }

    public function setProductService(ProductService $productService): void
    {
        $this->productService = $productService;
    }

    public function setStockinForm(StockinForm $stockinForm): void
    {
        $this->stockinForm = $stockinForm;
    }

    public function setStockService(StockService $stockService): void
    {
        $this->stockService = $stockService;
    }

    public function setViewOperatingLicenseItemForm(ViewOperatingLicenseItemForm $viewOperatingLicenseItemForm): void
    {
        $this->viewOperatingLicenseItemForm = $viewOperatingLicenseItemForm;
    }

    /**
     * @return JsonModel
     */
    public function factoryorderByNumberAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (empty($foNo = intval($this->params('fo_no')))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->factoryorderService->getFactoryorderByNumer($foNo));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function finishAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->getRequest()->isPost()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        $foEntity = new FactoryorderEntity();
        $uuid = new Uuid();
        $foUuid = $this->params('factoryorder_uuid');
        if (
            !$uuid->isValid($foUuid)
            || empty($foData = $this->factoryorderService->getFactoryorder($foUuid))
            || !$foEntity->exchangeArrayFromDatabase($foData)
            || !empty($foEntity->getFactoryorderTimeFinishReal())
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        // stock
        $postArray = $this->getRequest()->getPost()->toArray();
        $postArray['product_uuid'] = $foEntity->getProductUuid();
        $postArray['quantityunit_uuid'] = $foEntity->getQuantityunitUuid();
        $postArray['user_uuid_create'] = $this->userService->getUserUuid();
        $postArray['factoryorder_uuid'] = $foUuid;
        $this->stockinForm->setSecondaryKeysAvailable(false);
        $this->stockinForm->setStockinOrigin(StockService::STOCKIN_ORIGIN_FACTORYORDER);
        $this->stockinForm->init();
        $this->stockinForm->setData($postArray);
        if (!$this->stockinForm->isValid()) {
            $jsonModel->addMessages($this->stockinForm->getMessages());
        }
        $stockinEntity = new StockinEntity();
        if (!$stockinEntity->exchangeArrayFromDatabase($this->stockinForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if (!$foEntity->isValidFinishQuantity($stockinEntity->getStockinQuantity())) {
            $jsonModel->addMessage('stockin_quantity|#|Die Anzahl passt nicht zum bereits Produzierten');
        }
        if (!$this->productService->hasProductLocationCaseUuid($foEntity->getProductUuid(), $stockinEntity->getLocationCaseUuid())) {
            $jsonModel->addMessage('location_case_uuid|#|Der Lagerplatz gehört nicht zum Produkt.');
        }
        // operating license
        $licenseItemEntity = null;
        if (!empty($postArray['do_operating_license'])) {
            $licenseItemEntity = new ViewOperatingLicenseItemEntity();
            $this->viewOperatingLicenseItemForm->setOperatingLicenseSourceType(OperatingLicenseService::SOURCE_TYPE_STOCKIN);
            $this->viewOperatingLicenseItemForm->init();
            $this->viewOperatingLicenseItemForm->setData($postArray);
            if (!$this->viewOperatingLicenseItemForm->isValid()) {
                $jsonModel->addMessages($this->viewOperatingLicenseItemForm->getMessages());
            }
            if (!$licenseItemEntity->exchangeArrayFromDatabase($this->viewOperatingLicenseItemForm->getData())) {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
                return $jsonModel;
            }
        }
        if ($jsonModel->getMessagesCount() > 0) {
            return $jsonModel;
        }
        $stockinEntity->setStockinTimeApprove($this->factoryorderService->formatDateString($stockinEntity->getStockinTimeApprove()));
        if ($this->factoryorderService->finishFactoryorder($foEntity, $stockinEntity, $licenseItemEntity)) {
            $jsonModel->setSuccess(1);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function productStockinsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->getRequest()->isPost()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        $productUuids = filter_input_array(INPUT_POST, ['product_uuid' => [
            'filter' => FILTER_UNSAFE_RAW,
            'flags' => FILTER_REQUIRE_ARRAY | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH,
        ]]);
        $foUuid = filter_input(INPUT_POST, 'factoryorder_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $uuidV = new Uuid();
        if (empty($productUuids['product_uuid']) || !is_array($productUuids['product_uuid']) || !$uuidV->isValid($foUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        foreach ($productUuids['product_uuid'] as $productUuid) {
            if (!$uuidV->isValid($productUuid)) {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
                $jsonModel->addMessage('Product UUID not valid: ' . $productUuid);
                return $jsonModel;
            }
        }
        $jsonModel->setArr($this->factoryorderService->getProductsStockins($productUuids['product_uuid'], $foUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function isAutogenerateOperatingLicenseAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if ($this->factoryorderService->isAutogenerateOperatingLicense()) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}

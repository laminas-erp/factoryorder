<?php

namespace Lerp\Factoryorder\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Factoryorder\Service\FactoryorderService;

class OrderItemCreateFactoryorderAjaxController extends AbstractUserController
{
    protected FactoryorderService $factoryorderService;

    public function setFactoryorderService(FactoryorderService $factoryorderService): void
    {
        $this->factoryorderService = $factoryorderService;
    }

    /**
     * @return JsonModel
     */
    public function createAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        $data = $request->getPost()->toArray();
        if(!$request->isPost() || empty($data)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (
            !isset($data['order_item_uuid']) || !(new Uuid())->isValid($data['order_item_uuid'])
            || empty(intval($data['factoryorder_quantity']))
            || (isset($data['do_order_item_list']) && !in_array($data['do_order_item_list'], ['true', 'false']))
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->factoryorderService->createFactoryordersForOrderItem(
            $data['order_item_uuid'], $this->userService->getUserUuid(), $data['do_order_item_list'], intval($data['factoryorder_quantity']))
        ) {
            $jsonModel->setSuccess(1);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        } else {
            $jsonModel->addMessage($this->factoryorderService->getMessage());
        }
        return $jsonModel;
    }
}

<?php

namespace Lerp\Factoryorder\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Factoryorder\Service\FactoryorderService;

class OrderFactoryorderAjaxController extends AbstractUserController
{
    protected FactoryorderService $factoryorderService;

    public function setFactoryorderService(FactoryorderService $factoryorderService): void
    {
        $this->factoryorderService = $factoryorderService;
    }
	/**
	 * @return JsonModel
	 */
	public function orderFactoryordersAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderUuid = $this->params('order_uuid');
        if(!(new Uuid())->isValid($orderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->factoryorderService->getFactoryordersForOrder($orderUuid));
        $jsonModel->setSuccess(1);
		return $jsonModel;
	}
}

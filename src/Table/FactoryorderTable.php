<?php

namespace Lerp\Factoryorder\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Lerp\Factoryorder\Entity\ParamsFactoryorderEntity;

class FactoryorderTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'factoryorder';

    public static array $columnsStatic = [
        'factoryorder_uuid',
        'factoryorder_no',
        'product_uuid',
        'order_item_uuid',
        'order_item_list_uuid',
        'factoryorder_briefing',
        'factoryorder_quantity',
        'quantityunit_uuid',
        'location_place_uuid',
        'factoryorder_time_create',
        'factoryorder_time_update',
        'factoryorder_time_finish_schedule',
        'factoryorder_time_finish_real',
        'user_uuid_create',
        'user_uuid_update',
        'equipment_uuid',
    ];

    /**
     * @param string $factoryorderUuid
     * @return array Data from view_factoryorder.
     */
    public function getFactoryorder(string $factoryorderUuid): array
    {
        $select = new Select('view_factoryorder');
        try {
            $select->where(['factoryorder_uuid' => $factoryorderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getFactoryorderByNumber(int $factoryorderNo): array
    {
        $select = new Select('view_factoryorder');
        try {
            $select->where(['factoryorder_no' => $factoryorderNo]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productUuid
     * @param string $orderItemUuid
     * @param string $orderItemListUuid
     * @param string $briefing
     * @param int $quantity
     * @param string $quantityunitUuid
     * @param string $userUuid
     * @param string $locationPlaceUuid
     * @return string
     */
    public function insertFactoryorder(string $productUuid, string $orderItemUuid, string $orderItemListUuid, string $briefing,
                                       int    $quantity, string $quantityunitUuid, string $userUuid, string $locationPlaceUuid): string
    {
        $insert = $this->sql->insert();
        try {
            $uuid = $this->uuid();
            $insert->values([
                'factoryorder_uuid'     => $uuid,
                'product_uuid'          => $productUuid,
                'order_item_uuid'       => $orderItemUuid,
                'order_item_list_uuid'  => $orderItemListUuid ?: null,
                'factoryorder_briefing' => $briefing,
                'factoryorder_quantity' => $quantity,
                'quantityunit_uuid'     => $quantityunitUuid,
                'user_uuid_create'      => $userUuid,
                'location_place_uuid'   => $locationPlaceUuid,
            ]);
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $orderItemUuid
     * @return array Array from db.view_factoryorder.
     */
    public function getFactoryordersForOrderItem(string $orderItemUuid): array
    {
        $select = new Select('view_factoryorder');
        try {
            $select->where(['order_item_uuid' => $orderItemUuid]);
            $select->order('factoryorder_time_create ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getFactoryordersForOrder(string $orderUuid): array
    {
        $select = new Select('view_factoryorder');
        try {
            $select->where(['order_uuid' => $orderUuid]);
            $select->order('factoryorder_time_create ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param ParamsFactoryorderEntity $params
     * @return array Result rows or if doCount==true then an array with an integer.
     */
    public function searchFactoryorders(ParamsFactoryorderEntity $params): array
    {
        $select = new Select('view_factoryorder');
        try {
            if ($params->isDoCount()) {
                $select->columns(['count_fo' => new Expression('COUNT(factoryorder_uuid)')]);
            }
            $params->computeSelect($select);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if ($params->isDoCount()) {
                    return [intval($result->toArray()[0]['count_fo'])];
                } else {
                    return $result->toArray();
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [0];
    }

    /**
     * @param array $formData From form `\Lerp\Factoryorder\Form\FactoryorderForm`
     * @return int
     */
    public function updateFactoryorder(array $formData): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'factoryorder_briefing'             => $formData['factoryorder_briefing'],
                'factoryorder_quantity'             => $formData['factoryorder_quantity'],
                'factoryorder_time_update'          => new Expression('CURRENT_TIMESTAMP'),
                'factoryorder_time_finish_schedule' => $formData['factoryorder_time_finish_schedule'],
                'location_place_uuid'               => $formData['location_place_uuid'],
            ]);
            $update->where(['factoryorder_uuid' => $formData['factoryorder_uuid']]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateFactoryorderBriefing(string $factoryorderUuid, string $factoryorderBriefing): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['factoryorder_briefing' => $factoryorderBriefing]);
            $update->where(['factoryorder_uuid' => $factoryorderUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateFactoryorderFinish(string $factoryorderUuid, string $userUuid): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'factoryorder_time_finish_real' => new Expression('CURRENT_TIMESTAMP'),
                'user_uuid_update'              => $userUuid
            ]);
            $update->where(['factoryorder_uuid' => $factoryorderUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateFactoryorderEquipment(string $factoryorderUuid, string $equipUuid): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['equipment_uuid' => $equipUuid]);
            $update->where(['factoryorder_uuid' => $factoryorderUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}

<?php

namespace Lerp\Factoryorder\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class FactoryorderWorkflowTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'factoryorder_workflow';

    public static array $columnsStatic = [
        'factoryorder_workflow_uuid',
        'factoryorder_uuid',
        'workflow_uuid',
        'product_workflow_time',
        'factoryorder_workflow_time',
        'factoryorder_workflow_price_per_hour',
        'factoryorder_workflow_text',
        'factoryorder_workflow_order_priority',
        'factoryorder_workflow_time_create',
        'factoryorder_workflow_time_update',
    ];

    /**
     * @param string $foWorkflowUuid
     * @return array Data from db.view_factoryorder_workflow
     */
    public function getFactoryorderWorkflow(string $foWorkflowUuid): array
    {
        $select = new Select('view_factoryorder_workflow');
        try {
            $select->where(['factoryorder_workflow_uuid' => $foWorkflowUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $factoryorderUuid
     * @return array Data from db.view_factoryorder_workflow
     */
    public function getFactoryorderWorkflows(string $factoryorderUuid): array
    {
        $select = new Select('view_factoryorder_workflow');
        try {
            $select->where(['factoryorder_uuid' => $factoryorderUuid]);
            $select->order('factoryorder_workflow_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getMaxFactoryorderOrderPriority(string $factoryorderUuid): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_order_prio' => new Expression('MAX(factoryorder_workflow_order_priority)')]);
            $select->where(['factoryorder_uuid' => $factoryorderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return intval($result->toArray()[0]['max_order_prio']);
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return 1000;
    }

    /**
     * @param string $factoryorderUuid
     * @param string $workflowUuid
     * @param int $time
     * @param float $pricePerHour
     * @param string $text
     * @param int $orderPriority
     * @return string
     */
    public function insertFactoryorderWorkflow(string $factoryorderUuid, string $workflowUuid, int $time, float $pricePerHour, string $text, int $orderPriority): string
    {
        $insert = $this->sql->insert();
        try {
            $uuid = $this->uuid();
            $insert->values([
                'factoryorder_workflow_uuid'           => $uuid,
                'factoryorder_uuid'                    => $factoryorderUuid,
                'workflow_uuid'                        => $workflowUuid,
                'product_workflow_time'                => $time,
                'factoryorder_workflow_time'           => $time,
                'factoryorder_workflow_price_per_hour' => $pricePerHour,
                'factoryorder_workflow_text'           => $text,
                'factoryorder_workflow_order_priority' => $orderPriority,
            ]);
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateFactoryorderWorkflow(string $foWorkflowUuid, string $workflowUuid, int $workflowTime, float $pricePerHour, int $orderPriority, string $workflowText): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'workflow_uuid'                        => $workflowUuid,
                'factoryorder_workflow_time'           => $workflowTime,
                'factoryorder_workflow_price_per_hour' => $pricePerHour,
                'factoryorder_workflow_order_priority' => $orderPriority,
                'factoryorder_workflow_time_update'    => new Expression('CURRENT_TIMESTAMP'),
                'factoryorder_workflow_text'           => $workflowText,
            ]);
            $update->where(['factoryorder_workflow_uuid' => $foWorkflowUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $foWorkflowUuid
     * @return int
     */
    public function deleteFactoryorderWorkflow(string $foWorkflowUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['factoryorder_workflow_uuid' => $foWorkflowUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getFactoryorderWorkflowsByFactoryorderWorkflowUuid(string $foWorkflowUuid): array
    {
        $foWorkflow = $this->getFactoryorderWorkflow($foWorkflowUuid);
        if (empty($foWorkflow)) {
            return [];
        }
        return $this->getFactoryorderWorkflows($foWorkflow['factoryorder_uuid']);
    }

    public function updateFactoryorderWorkflowOrderPriority(string $foWorkflowUuid, string $direc): bool
    {
        $neighbors = $this->getFactoryorderWorkflowsByFactoryorderWorkflowUuid($foWorkflowUuid);
        if (empty($neighbors)) {
            false;
        }
        foreach ($neighbors as $i => $neighbor) {
            if ($neighbor['factoryorder_workflow_uuid'] == $foWorkflowUuid) {
                $self = $neighbor;
                $top = isset($neighbors[$i - 1]) ? $neighbors[$i - 1] : null;
                $down = isset($neighbors[$i + 1]) ? $neighbors[$i + 1] : null;
            }
        }
        if ($direc === 'up') {
            if (!isset($top)) {
                return false;
            }
            $this->updateFactoryorderWorkflowOrderPriorityConcrete($top['factoryorder_workflow_uuid'], $self['factoryorder_workflow_order_priority']);
            $this->updateFactoryorderWorkflowOrderPriorityConcrete($self['factoryorder_workflow_uuid'], $top['factoryorder_workflow_order_priority']);
        } else if ($direc === 'down') {
            if (!isset($down)) {
                return false;
            }
            $this->updateFactoryorderWorkflowOrderPriorityConcrete($down['factoryorder_workflow_uuid'], $self['factoryorder_workflow_order_priority']);
            $this->updateFactoryorderWorkflowOrderPriorityConcrete($self['factoryorder_workflow_uuid'], $down['factoryorder_workflow_order_priority']);
        }
        return true;
    }

    /**
     * @param string $foWorkflowUuid
     * @param int $orderPriority
     * @return int
     */
    public function updateFactoryorderWorkflowOrderPriorityConcrete(string $foWorkflowUuid, int $orderPriority): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'factoryorder_workflow_order_priority' => $orderPriority,
                'factoryorder_workflow_time_update'    => new Expression('CURRENT_TIMESTAMP')
            ]);
            $update->where(['factoryorder_workflow_uuid' => $foWorkflowUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $foUuid
     * @param int $orderPriority
     * @param bool $ifUp
     * @return array
     */
    public function getFactoryorderWorkflowsForOrderPriority(string $foUuid, int $orderPriority, bool $ifUp): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['factoryorder_uuid' => $foUuid]);
            if ($ifUp) {
                $select->where->lessThanOrEqualTo('factoryorder_workflow_order_priority', $orderPriority);
                $select->order('factoryorder_workflow_order_priority DESC');
            } else {
                $select->where->greaterThanOrEqualTo('factoryorder_workflow_order_priority', $orderPriority);
                $select->order('factoryorder_workflow_order_priority ASC');
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $workflowUuid
     * @param string $workflowUuidReplace
     * @return int
     */
    public function updateWorkflowUuid(string $workflowUuid, string $workflowUuidReplace): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['workflow_uuid' => $workflowUuidReplace]);
            $update->where(['workflow_uuid' => $workflowUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}

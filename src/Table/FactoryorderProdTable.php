<?php

namespace Lerp\Factoryorder\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class FactoryorderProdTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'factoryorder_prod';

    /**
     * @param string $factoryorderProdUuid
     * @return array
     */
    public function getFactoryorderProd(string $factoryorderProdUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['factoryorder_prod_uuid' => $factoryorderProdUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $factoryorderUuid
     * @param int $finishQntty
     * @param string $userUuid
     * @return string
     */
    public function insertFactoryorderProd(string $factoryorderUuid, int $finishQntty, string $userUuid): string
    {
        $insert = $this->sql->insert();
        try {
            $uuid = $this->uuid();
            $insert->values([
                'factoryorder_prod_uuid' => $uuid,
                'factoryorder_uuid' => $factoryorderUuid,
                'factoryorder_prod_quantity' => $finishQntty,
                'user_uuid' => $userUuid,
            ]);
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }
}

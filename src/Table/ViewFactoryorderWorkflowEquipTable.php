<?php

namespace Lerp\Factoryorder\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Predicate\Expression;
use Lerp\Factoryorder\Entity\ParamsFactoryorderWorkflowEquipEntity;

class ViewFactoryorderWorkflowEquipTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_factoryorder_workflow_equip';

    /**
     * @param string $equipmentUuid UUID from the main person e.g. project manager
     * @return array
     */
    public function getFactoryorderWorkflowsEquip(string $equipmentUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['equipment_uuid' => $equipmentUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function searchFactoryorderWorkflowsEquip(ParamsFactoryorderWorkflowEquipEntity $params): array
    {
        $select = $this->sql->select();
        try {
            if($params->isDoCount()) {
                $select->columns(['count_fo' => new Expression('COUNT(factoryorder_workflow_uuid)')]);
            }
            $params->computeSelect($select);
            $s = $select->getSqlString($this->adapter->platform);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $r = $result->toArray();
                if($params->isDoCount()) {
                    return [intval($r[0]['count_fo'])];
                } else {
                    return $r;
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

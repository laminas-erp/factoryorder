<?php

namespace Lerp\Factoryorder\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Lerp\Factoryorder\Table\FactoryorderProdTable;
use Lerp\Factoryorder\Table\FactoryorderTable;
use Lerp\Factoryorder\Table\FactoryorderWorkflowTable;

class FactoryorderGodService extends AbstractService
{
    protected FactoryorderTable $factoryorderTable;
    protected FactoryorderWorkflowTable $factoryorderWorkflowTable;
    protected FactoryorderProdTable $factoryorderProdTable;
}

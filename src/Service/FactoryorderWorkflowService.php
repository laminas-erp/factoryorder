<?php

namespace Lerp\Factoryorder\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Laminas\Log\Logger;
use Lerp\Factoryorder\Table\FactoryorderTable;
use Lerp\Factoryorder\Table\FactoryorderWorkflowTable;
use Lerp\Product\Table\Workflow\WorkflowTable;

class FactoryorderWorkflowService extends AbstractService
{
    protected FactoryorderTable $factoryorderTable;
    protected FactoryorderWorkflowTable $factoryorderWorkflowTable;
    protected WorkflowTable $workflowTable;

    public function setFactoryorderTable(FactoryorderTable $factoryorderTable): void
    {
        $this->factoryorderTable = $factoryorderTable;
    }

    public function setFactoryorderWorkflowTable(FactoryorderWorkflowTable $factoryorderWorkflowTable): void
    {
        $this->factoryorderWorkflowTable = $factoryorderWorkflowTable;
    }

    public function setWorkflowTable(WorkflowTable $workflowTable): void
    {
        $this->workflowTable = $workflowTable;
    }

    /**
     * Insert a factoryorder workflow with order priority MAX+10.
     *
     * @param string $factoryorderUuid
     * @param string $workflowUuid
     * @param float $pricePerHour
     * @return string
     */
    public function insertFactoryorderWorkflow(string $factoryorderUuid, string $workflowUuid, float $pricePerHour = 0): string
    {
        $wf = $this->workflowTable->getWorkflowFromView($workflowUuid);
        return $this->factoryorderWorkflowTable->insertFactoryorderWorkflow($factoryorderUuid, $workflowUuid, 0, $pricePerHour, $wf['workflow_text']
            , $this->factoryorderWorkflowTable->getMaxFactoryorderOrderPriority($factoryorderUuid) + 10);
    }

    /**
     * @param string $factoryorderUuid
     * @return array From db.view_factoryorder_workflow
     */
    public function getFactoryorderWorkflows(string $factoryorderUuid): array
    {
        return $this->factoryorderWorkflowTable->getFactoryorderWorkflows($factoryorderUuid);
    }

    public function deleteFactoryorderWorkflow(string $foWorkflowUuid): bool
    {
        return $this->factoryorderWorkflowTable->deleteFactoryorderWorkflow($foWorkflowUuid);
    }

    public function updateFactoryorderWorkflow(string $foWorkflowUuid, string $workflowUuid, int $workflowTime, float $pricePerHour, int $orderPriority, string $workflowText): bool
    {
        return $this->factoryorderWorkflowTable->updateFactoryorderWorkflow($foWorkflowUuid, $workflowUuid, $workflowTime, $pricePerHour, $orderPriority, $workflowText) >= 0;
    }

    /**
     * Move factoryorderWorkflow one step in a direction ('up' | 'down').
     * @param string $foWorkflowUuid
     * @param string $direc
     * @return bool
     */
    public function updateFactoryorderWorkflowOrderPriorityMove(string $foWorkflowUuid, string $direc): bool
    {
        return $this->factoryorderWorkflowTable->updateFactoryorderWorkflowOrderPriority($foWorkflowUuid, $direc) >= 0;
    }

    public function updateFactoryorderWorkflowOrderPriority(string $foWorkflowUuid, int $prioPrevious, int $prioCurrent): bool
    {
        if ($prioPrevious == $prioCurrent) {
            return false;
        }
        $foWorkflow = $this->factoryorderWorkflowTable->getFactoryorderWorkflow($foWorkflowUuid);
        if (empty($foWorkflow)) {
            return false;
        }
        $conn = $this->beginTransaction($this->factoryorderWorkflowTable);

        $success = true;
        $orderPrio = $prioCurrent;
        if ($prioPrevious > $prioCurrent) {
            // down
            $items = $this->factoryorderWorkflowTable->getFactoryorderWorkflowsForOrderPriority($foWorkflow['factoryorder_uuid'], $prioCurrent, false);
            foreach ($items as $item) {
                $orderPrio += 10;
                if ($this->factoryorderWorkflowTable->updateFactoryorderWorkflowOrderPriorityConcrete($item['factoryorder_workflow_uuid'], $orderPrio) < 0) {
                    $success = false;
                }
            }
        } else {
            // up
            $items = $this->factoryorderWorkflowTable->getFactoryorderWorkflowsForOrderPriority($foWorkflow['factoryorder_uuid'], $prioCurrent, true);
            foreach ($items as $item) {
                $orderPrio -= 10;
                if ($this->factoryorderWorkflowTable->updateFactoryorderWorkflowOrderPriorityConcrete($item['factoryorder_workflow_uuid'], $orderPrio) < 0) {
                    $success = false;
                }
            }
        }
        if ($success && $this->factoryorderWorkflowTable->updateFactoryorderWorkflowOrderPriorityConcrete($foWorkflowUuid, $prioCurrent) >= 0) {
            $conn->commit();
            return true;
        } else {
            $conn->rollback();
            return false;
        }
    }
}

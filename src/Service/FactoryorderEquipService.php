<?php

namespace Lerp\Factoryorder\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Lerp\Equipment\Service\Equipment\EquipmentService;
use Lerp\Factoryorder\Entity\ParamsFactoryorderWorkflowEquipEntity;
use Lerp\Factoryorder\Table\FactoryorderTable;
use Lerp\Factoryorder\Table\ViewFactoryorderWorkflowEquipTable;

class FactoryorderEquipService extends AbstractService
{
    protected FactoryorderTable $factoryorderTable;
    protected ViewFactoryorderWorkflowEquipTable $viewFactoryorderWorkflowEquipTable;
    protected EquipmentService $equipmentService;
    protected int $searchFactoryorderCount = 0;

    public function setFactoryorderTable(FactoryorderTable $factoryorderTable): void
    {
        $this->factoryorderTable = $factoryorderTable;
    }

    public function setViewFactoryorderWorkflowEquipTable(ViewFactoryorderWorkflowEquipTable $viewFactoryorderWorkflowEquipTable): void
    {
        $this->viewFactoryorderWorkflowEquipTable = $viewFactoryorderWorkflowEquipTable;
    }

    public function setEquipmentService(EquipmentService $equipmentService): void
    {
        $this->equipmentService = $equipmentService;
    }

    public function getSearchFactoryorderCount(): int
    {
        return $this->searchFactoryorderCount;
    }

    /**
     * @param ParamsFactoryorderWorkflowEquipEntity $params
     * @return array From db.view_factoryorder_workflow_equip
     */
    public function searchFactoryorderWorkflowsEquip(ParamsFactoryorderWorkflowEquipEntity $params): array
    {
        $params->setDoCount(true);
        $this->searchFactoryorderCount = $this->viewFactoryorderWorkflowEquipTable->searchFactoryorderWorkflowsEquip($params)[0];
        $params->setDoCount(false);
        return $this->viewFactoryorderWorkflowEquipTable->searchFactoryorderWorkflowsEquip($params);
    }

    /**
     * @param string $factoryorderUuid
     * @param string $userUuid
     * @return bool
     */
    public function updateFactoryorderEquipmentUser(string $factoryorderUuid, string $userUuid): bool
    {
        if(empty($equip = $this->equipmentService->getEquipmentByUser($userUuid))) {
            return false;
        }
        return $this->updateFactoryorderEquipment($factoryorderUuid, $equip['equipment_uuid']);
    }

    public function updateFactoryorderEquipment(string $factoryorderUuid, string $equipUuid): bool
    {
        return $this->factoryorderTable->updateFactoryorderEquipment($factoryorderUuid, $equipUuid) >= 0;
    }
}

<?php

namespace Lerp\Factoryorder\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\ConnectionInterface;
use Laminas\Stdlib\ArrayUtils;
use Lerp\Factoryorder\Entity\FactoryorderEntity;
use Lerp\Factoryorder\Entity\OrderItemListFactoryorderEntity;
use Lerp\Factoryorder\Entity\ParamsFactoryorderEntity;
use Lerp\Factoryorder\Table\FactoryorderProdTable;
use Lerp\Factoryorder\Table\FactoryorderTable;
use Lerp\Factoryorder\Table\FactoryorderWorkflowTable;
use Lerp\OperatingLicense\Entity\ViewOperatingLicenseItemEntity;
use Lerp\OperatingLicense\Service\OperatingLicenseAccumulateService;
use Lerp\Order\Service\Order\OrderItemListService;
use Lerp\Order\Service\Order\OrderItemService;
use Lerp\Product\Service\ProductService;
use Lerp\Product\Service\ProductWorkflowService;
use Lerp\Stock\Entity\StockinEntity;
use Lerp\Stock\Service\StockService;

class FactoryorderService extends AbstractService
{
    protected bool $isAutogenerateOperatingLicense;
    protected FactoryorderTable $factoryorderTable;
    protected FactoryorderWorkflowTable $factoryorderWorkflowTable;
    protected FactoryorderProdTable $factoryorderProdTable;
    protected OrderItemService $orderItemService;
    protected OrderItemListService $orderItemListService;
    protected ProductService $productService;
    protected ProductWorkflowService $productWorkflowService;
    protected StockService $stockService;
    protected ConnectionInterface $connection;
    protected string $userUuid;
    protected int $searchFactoryorderCount = 0;
    protected OperatingLicenseAccumulateService $operatingLicenseAccumulateService;

    protected array $productStructures = ['standard', 'list', 'service', 'maintenance'];

    public function setIsAutogenerateOperatingLicense(bool $isAutogenerateOperatingLicense): void
    {
        $this->isAutogenerateOperatingLicense = $isAutogenerateOperatingLicense;
    }

    public function isAutogenerateOperatingLicense(): bool
    {
        return $this->isAutogenerateOperatingLicense;
    }

    public function setFactoryorderTable(FactoryorderTable $factoryorderTable): void
    {
        $this->factoryorderTable = $factoryorderTable;
        /** @var Adapter $adapter */
        $adapter = $this->factoryorderTable->getAdapter();
        $this->connection = $adapter->getDriver()->getConnection();
    }

    public function setFactoryorderWorkflowTable(FactoryorderWorkflowTable $factoryorderWorkflowTable): void
    {
        $this->factoryorderWorkflowTable = $factoryorderWorkflowTable;
    }

    public function setFactoryorderProdTable(FactoryorderProdTable $factoryorderProdTable): void
    {
        $this->factoryorderProdTable = $factoryorderProdTable;
    }

    public function setOrderItemService(OrderItemService $orderItemService): void
    {
        $this->orderItemService = $orderItemService;
    }

    public function setOrderItemListService(OrderItemListService $orderItemListService): void
    {
        $this->orderItemListService = $orderItemListService;
    }

    public function setProductService(ProductService $productService): void
    {
        $this->productService = $productService;
    }

    public function setProductWorkflowService(ProductWorkflowService $productWorkflowService): void
    {
        $this->productWorkflowService = $productWorkflowService;
    }

    public function setStockService(StockService $stockService): void
    {
        $this->stockService = $stockService;
    }

    public function getSearchFactoryorderCount(): int
    {
        return $this->searchFactoryorderCount;
    }

    public function setOperatingLicenseAccumulateService(OperatingLicenseAccumulateService $operatingLicenseAccumulateService): void
    {
        $this->operatingLicenseAccumulateService = $operatingLicenseAccumulateService;
    }

    /**
     * Create a factoryorder for the orderItem.
     * factoryorder_quantity = order_item_quantity
     * Look for the orderItemList in the stock and create factory orders accordingly.
     * ...looks deep into the list.
     * @param string $orderItemUuid
     * @param string $userUuid
     * @param bool $doOrderItemList
     * @param int $qntty If empty then it will use order_item_quantity.
     * @return bool
     */
    public function createFactoryordersForOrderItem(string $orderItemUuid, string $userUuid, bool $doOrderItemList, int $qntty = 0): bool
    {
        $oi = $this->orderItemService->getOrderItemForFactoryorder($orderItemUuid);
        if (empty($oi)) {
            return false;
        }
        if (!in_array($oi['product_structure'], $this->productStructures)) {
            $this->message = 'Die Produkt Struktur "' . $oi['product_structure'] . '" ist nicht geeignet für einen Betriebsauftrag!';
            return false;
        }
        if (!empty($qntty)) {
            $oi['order_item_quantity'] = $qntty;
        }
        $this->connection->beginTransaction();
        $this->userUuid = $userUuid;
        $foUuid = $this->factoryorderTable->insertFactoryorder($oi['product_uuid'], $orderItemUuid, '', $oi['product_briefing'], $oi['order_item_quantity']
            , $oi['quantityunit_uuid'], $this->userUuid, $oi['location_place_uuid']);
        if (empty($foUuid) || !is_string($foUuid)) {
            $this->connection->rollback();
            return false;
        }
        if ($oi['count_workflow'] > 0 && !$this->createWorkflows($foUuid, $oi['product_uuid'])) {
            $this->connection->rollback();
            return false;
        }
        $this->connection->commit();
        if ($oi['count_order_item_list'] > 0 && $doOrderItemList) {
            // first level OrderItemList
            $oils = $this->orderItemListService->getOrderItemListLevelForFactoryorder($orderItemUuid, $oi['product_uuid']);
            $oilfe = new OrderItemListFactoryorderEntity();
            foreach ($oils as $oil) {
                if (!in_array($oil['product_structure'], $this->productStructures)) {
                    continue;
                }
                if ((($diff = $this->stockService->getStockDifference($oil['product_uuid'], $oil['order_item_list_quantity'], $oil['quantityunit_uuid'])) < 0)) {
                    $oilfe->exchangeArrayFromDatabase($oil);
                    $oilfe->setOrderItemListQuantity(ceil(abs($diff)));
                    $this->createFactoryordersForOrderItemList($oilfe);
                }
            }
        }
        return true;
    }

    public function createFactoryordersForOrderItemLists(array $oilUuidQntty, string $userUuid): bool
    {
        $this->userUuid = $userUuid;
        foreach ($oilUuidQntty as $oilUuid => $qntty) {
            $oilfe = new OrderItemListFactoryorderEntity();
            if (!$oilfe->exchangeArrayFromDatabase($this->orderItemListService->getOrderItemListForFactoryorder($oilUuid))) {
                return false;
            }
            if (!$this->createFactoryordersForOrderItemList($oilfe)) {
                return false;
            }
        }
        return true;
    }

    protected function createFactoryordersForOrderItemList(OrderItemListFactoryorderEntity $oilfe): bool
    {
        $this->connection->beginTransaction();
        $foUuid = $this->factoryorderTable->insertFactoryorder($oilfe->getProductUuid(), $oilfe->getOrderItemUuid(), $oilfe->getOrderItemListUuid(), $oilfe->getProductBriefing(),
            $oilfe->getOrderItemListQuantity(), $oilfe->getQuantityunitUuid(), $this->userUuid, $oilfe->getLocationPlaceUuid());
        if (empty($foUuid)) {
            $this->connection->rollback();
            return false;
        }
        if ($oilfe->getCountWorkflow() > 0 && !$this->createWorkflows($foUuid, $oilfe->getProductUuid())) {
            $this->connection->rollback();
            return false;
        }
        $this->connection->commit();

        if ($oilfe->getCountOrderItemList() > 0) {
            // n'te level OrderItemList
            $oils = $this->orderItemListService->getOrderItemListLevelForFactoryorder($oilfe->getOrderItemUuid(), $oilfe->getProductUuid());
            $oilfeInner = new OrderItemListFactoryorderEntity();
            foreach ($oils as $oil) {
                if (!in_array($oil['product_structure'], $this->productStructures)) {
                    continue;
                }
                if ((($diff = $this->stockService->getStockDifference($oil['product_uuid'], $oil['order_item_list_quantity'], $oil['quantityunit_uuid'])) < 0)) {
                    $oilfeInner->exchangeArrayFromDatabase($oil);
                    $oilfeInner->setOrderItemListQuantity(ceil(abs($diff)));
                    $this->createFactoryordersForOrderItemList($oilfeInner); // recursion
                }
            }
        }
        return true;
    }

    protected function createWorkflows(string $factoryOrderUuid, string $productUuid): bool
    {
        $pws = $this->productWorkflowService->getProductWorkflows($productUuid);
        if (empty($pws)) {
            return false;
        }
        $foWorkflowUuids = [];
        foreach ($pws as $pw) {
            if (empty($foWorkflowUuids[] = $this->factoryorderWorkflowTable->insertFactoryorderWorkflow($factoryOrderUuid, $pw['workflow_uuid'], $pw['product_workflow_time'], $pw['product_workflow_price_per_hour'], $pw['product_workflow_text'], $pw['product_workflow_order_priority']))) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param string $orderItemUuid
     * @return array From db.view_factoryorder
     */
    public function getFactoryordersForOrderItem(string $orderItemUuid): array
    {
        return $this->factoryorderTable->getFactoryordersForOrderItem($orderItemUuid);
    }

    /**
     * @param string $orderUuid
     * @return array From db.view_factoryorder.
     */
    public function getFactoryordersForOrder(string $orderUuid): array
    {
        return $this->factoryorderTable->getFactoryordersForOrder($orderUuid);
    }

    /**
     * @param string $foUuid
     * @return array From view_factoryorder.
     */
    public function getFactoryorder(string $foUuid): array
    {
        return $this->factoryorderTable->getFactoryorder($foUuid);
    }

    public function getFactoryorderByNumer(int $foNo): array
    {
        return $this->factoryorderTable->getFactoryorderByNumber($foNo);
    }

    public function searchFactoryorders(ParamsFactoryorderEntity $params): array
    {
        $params->setDoCount(true);
        $this->searchFactoryorderCount = $this->factoryorderTable->searchFactoryorders($params)[0];
        $params->setDoCount(false);
        return $this->factoryorderTable->searchFactoryorders($params);
    }

    /**
     * @param array $formData From form `\Lerp\Factoryorder\Form\FactoryorderForm`
     * @return bool
     */
    public function updateFactoryorder(array $formData): bool
    {
        return $this->factoryorderTable->updateFactoryorder($formData) >= 0;
    }

    public function updateFactoryorderBriefing(string $factoryorderUuid, string $factoryorderBriefing): bool
    {
        return $this->factoryorderTable->updateFactoryorderBriefing($factoryorderUuid, $factoryorderBriefing) >= 0;
    }

    /**
     * @param FactoryorderEntity $foEntity
     * @param StockinEntity $stockinEntity
     * @param ViewOperatingLicenseItemEntity|null $licenseItemEntity
     * @return bool
     */
    public function finishFactoryorder(FactoryorderEntity $foEntity, StockinEntity $stockinEntity, ViewOperatingLicenseItemEntity $licenseItemEntity = null): bool
    {
        $conn = $this->beginTransaction($this->factoryorderTable);
        $finishQntty = $stockinEntity->getStockinQuantity();
        $userUuid = $stockinEntity->getUserUuidCreate();
        if (empty($this->factoryorderProdTable->insertFactoryorderProd($foEntity->getUuid(), $finishQntty, $userUuid))) {
            $conn->rollback();
            return false;
        }
        if ($foEntity->getStillToBeProduced() == $finishQntty) {
            if (!$this->factoryorderTable->updateFactoryorderFinish($foEntity->getUuid(), $userUuid)) {
                $conn->rollback();
                return false;
            }
        }
        if (empty($stockinUuid = $this->stockService->insertStockinWithEntity($stockinEntity))) {
            $conn->rollback();
            return false;
        }
        if ($this->isAutogenerateOperatingLicense && isset($licenseItemEntity)) {
            $licenseItemEntity->setStockinUuid($stockinUuid);
            if ($licenseItemEntity->isCompleteForInsert() && !$this->operatingLicenseAccumulateService->insertAndCreateOperatingLicense($licenseItemEntity)) {
                $conn->rollback();
                return false;
            }
        }
        $conn->commit();
        return true;
    }

    /**
     * @param array $productUuids
     * @param string $factoryorderUuid To fetch the factoryorder with location_place_uuid.
     * @return array All stockins for the product and the location from the factoryorder.
     */
    public function getProductsStockins(array $productUuids, string $factoryorderUuid): array
    {
        $fo = $this->factoryorderTable->getFactoryorder($factoryorderUuid);
        if (empty($fo)) {
            return [];
        }
        $stockins = [];
        foreach ($productUuids as $productUuid) {
            $psi = $this->stockService->getProductStockinsForLocation($productUuid, $fo['location_place_uuid']);
            if (empty($psi)) {
                continue;
            }
            $stockins = ArrayUtils::merge($stockins, $psi);
        }
        return $stockins;
    }
}

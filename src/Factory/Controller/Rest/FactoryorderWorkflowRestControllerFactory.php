<?php

namespace Lerp\Factoryorder\Factory\Controller\Rest;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\FactoryorderDocumentService;
use Lerp\Factoryorder\Controller\Rest\FactoryorderWorkflowRestController;
use Lerp\Factoryorder\Form\FactoryorderWorkflowForm;
use Lerp\Factoryorder\Service\FactoryorderService;
use Lerp\Factoryorder\Service\FactoryorderWorkflowService;

class FactoryorderWorkflowRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FactoryorderWorkflowRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFactoryorderService($container->get(FactoryorderService::class));
        $controller->setFactoryorderWorkflowService($container->get(FactoryorderWorkflowService::class));
        $controller->setFactoryorderWorkflowForm($container->get(FactoryorderWorkflowForm::class));
        $controller->setFactoryorderDocumentService($container->get(FactoryorderDocumentService::class));
        return $controller;
    }
}

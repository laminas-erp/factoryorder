<?php

namespace Lerp\Factoryorder\Factory\Controller\Rest;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Factoryorder\Controller\Rest\FactoryorderRestController;
use Lerp\Factoryorder\Form\FactoryorderForm;
use Lerp\Factoryorder\Service\FactoryorderService;

class FactoryorderRestControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$controller = new FactoryorderRestController();
		$controller->setLogger($container->get('logger'));
		$controller->setUserService($container->get(UserService::class));
        $controller->setFactoryorderService($container->get(FactoryorderService::class));
        $controller->setFactoryorderForm($container->get(FactoryorderForm::class));
		return $controller;
	}
}

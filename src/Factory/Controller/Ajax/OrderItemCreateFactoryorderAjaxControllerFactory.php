<?php

namespace Lerp\Factoryorder\Factory\Controller\Ajax;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Factoryorder\Controller\Ajax\OrderItemCreateFactoryorderAjaxController;
use Lerp\Factoryorder\Service\FactoryorderService;
use Psr\Container\NotFoundExceptionInterface;

class OrderItemCreateFactoryorderAjaxControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return OrderItemCreateFactoryorderAjaxController
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): OrderItemCreateFactoryorderAjaxController
    {
        $controller = new OrderItemCreateFactoryorderAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFactoryorderService($container->get(FactoryorderService::class));
        return $controller;
    }
}

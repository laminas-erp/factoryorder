<?php

namespace Lerp\Factoryorder\Factory\Controller\Ajax;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Factoryorder\Controller\Ajax\FactoryorderAjaxController;
use Lerp\Factoryorder\Service\FactoryorderService;
use Lerp\OperatingLicense\Form\ViewOperatingLicenseItemForm;
use Lerp\Product\Service\ProductService;
use Lerp\Stock\Form\StockinForm;
use Lerp\Stock\Service\StockService;

class FactoryorderAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FactoryorderAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFactoryorderService($container->get(FactoryorderService::class));
        $controller->setProductService($container->get(ProductService::class));
        $controller->setStockinForm($container->get(StockinForm::class));
        $controller->setStockService($container->get(StockService::class));
        $controller->setViewOperatingLicenseItemForm($container->get(ViewOperatingLicenseItemForm::class));
        return $controller;
    }
}

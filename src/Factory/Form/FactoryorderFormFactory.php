<?php

namespace Lerp\Factoryorder\Factory\Form;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Factoryorder\Form\FactoryorderForm;
use Lerp\Location\Table\LocationPlaceTable;

class FactoryorderFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new FactoryorderForm();
        /** @var LocationPlaceTable $lpt */
        $lpt = $container->get(LocationPlaceTable::class);
        $form->setLocationPlaceUuidAssoc($lpt->getLocationPlacesUuidAssoc());
        return $form;
    }
}

<?php

namespace Lerp\Factoryorder\Factory\Service;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Factoryorder\Service\FactoryorderService;
use Lerp\Factoryorder\Table\FactoryorderProdTable;
use Lerp\Factoryorder\Table\FactoryorderTable;
use Lerp\Factoryorder\Table\FactoryorderWorkflowTable;
use Lerp\OperatingLicense\Service\OperatingLicenseAccumulateService;
use Lerp\Order\Service\Order\OrderItemListService;
use Lerp\Order\Service\Order\OrderItemService;
use Lerp\Product\Service\ProductService;
use Lerp\Product\Service\ProductWorkflowService;
use Lerp\Stock\Service\StockService;

class FactoryorderServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FactoryorderService();
        $service->setLogger($container->get('logger'));
        $service->setIsAutogenerateOperatingLicense($container->get('config')['lerp_factoryorder']['auto_generate_operating_license']);
        $service->setFactoryorderTable($container->get(FactoryorderTable::class));
        $service->setFactoryorderWorkflowTable($container->get(FactoryorderWorkflowTable::class));
        $service->setFactoryorderProdTable($container->get(FactoryorderProdTable::class));
        $service->setOrderItemService($container->get(OrderItemService::class));
        $service->setOrderItemListService($container->get(OrderItemListService::class));
        $service->setProductService($container->get(ProductService::class));
        $service->setProductWorkflowService($container->get(ProductWorkflowService::class));
        $service->setStockService($container->get(StockService::class));
        $service->setOperatingLicenseAccumulateService($container->get(OperatingLicenseAccumulateService::class));
        return $service;
    }
}

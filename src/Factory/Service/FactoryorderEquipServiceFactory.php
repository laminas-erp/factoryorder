<?php

namespace Lerp\Factoryorder\Factory\Service;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Equipment\Service\Equipment\EquipmentService;
use Lerp\Factoryorder\Service\FactoryorderEquipService;
use Lerp\Factoryorder\Table\FactoryorderTable;
use Lerp\Factoryorder\Table\ViewFactoryorderWorkflowEquipTable;

class FactoryorderEquipServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FactoryorderEquipService();
        $service->setLogger($container->get('logger'));
        $service->setFactoryorderTable($container->get(FactoryorderTable::class));
        $service->setViewFactoryorderWorkflowEquipTable($container->get(ViewFactoryorderWorkflowEquipTable::class));
        $service->setEquipmentService($container->get(EquipmentService::class));
        return $service;
    }
}

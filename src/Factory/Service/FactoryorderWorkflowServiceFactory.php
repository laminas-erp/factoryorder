<?php

namespace Lerp\Factoryorder\Factory\Service;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Factoryorder\Service\FactoryorderWorkflowService;
use Lerp\Factoryorder\Table\FactoryorderTable;
use Lerp\Factoryorder\Table\FactoryorderWorkflowTable;
use Lerp\Product\Table\Workflow\WorkflowTable;

class FactoryorderWorkflowServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FactoryorderWorkflowService();
        $service->setLogger($container->get('logger'));
        $service->setFactoryorderTable($container->get(FactoryorderTable::class));
        $service->setFactoryorderWorkflowTable($container->get(FactoryorderWorkflowTable::class));
        $service->setWorkflowTable($container->get(WorkflowTable::class));
        return $service;
    }
}

<?php

namespace Lerp\Factoryorder\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

/**
 * Class FactoryorderEntity
 * Data from db.view_factoryorder.
 * @package Lerp\Factoryorder\Entity
 */
class FactoryorderEntity extends AbstractEntity
{
    public array $mapping = [
        'factoryorder_uuid'                 => 'factoryorder_uuid',
        'factoryorder_no'                   => 'factoryorder_no',
        'product_uuid'                      => 'product_uuid',
        'order_item_uuid'                   => 'order_item_uuid',
        'order_item_list_uuid'              => 'order_item_list_uuid',
        'factoryorder_briefing'             => 'factoryorder_briefing',
        'factoryorder_quantity'             => 'factoryorder_quantity',
        'factoryorder_time_create'          => 'factoryorder_time_create',
        'factoryorder_time_update'          => 'factoryorder_time_update',
        'location_place_uuid'               => 'location_place_uuid',
        'factoryorder_time_create_unix'     => 'factoryorder_time_create_unix',
        'factoryorder_time_finish_schedule' => 'factoryorder_time_finish_schedule',
        'factoryorder_time_finish_real'     => 'factoryorder_time_finish_real',
        'user_uuid_create'                  => 'user_uuid_create',
        'user_uuid_update'                  => 'user_uuid_update',
        'cost_centre_id'                    => 'cost_centre_id',
        'order_uuid'                        => 'order_uuid',
        'order_no'                          => 'order_no',
        'order_no_compl'                    => 'order_no_compl',
        'quantityunit_uuid'                 => 'quantityunit_uuid',
        'quantityunit_name'                 => 'quantityunit_name',
        'quantityunit_label'                => 'quantityunit_label',
        'order_time_finish_schedule'        => 'order_time_finish_schedule',
        'location_place_name'               => 'location_place_name',
        'location_place_label'              => 'location_place_label',
        'customer_no'                       => 'customer_no',
        'customer_name'                     => 'customer_name',
        'user_details_name_first'           => 'user_details_name_first',
        'user_details_name_last'            => 'user_details_name_last',
        'product_structure'                 => 'product_structure',
        'product_origin'                    => 'product_origin',
        'product_type'                      => 'product_type',
        'product_text_part'                 => 'product_text_part',
        'product_text_short'                => 'product_text_short',
        'product_text_long'                 => 'product_text_long',
        'product_text_license'              => 'product_text_license',
        'product_text_license_remark'       => 'product_text_license_remark',
        'product_no_no'                     => 'product_no_no',
        'count_workflow'                    => 'count_workflow',
        'count_location'                    => 'count_location',
        'sum_prod_quantity'                 => 'sum_prod_quantity',
    ];

    protected $primaryKey = 'factoryorder_uuid';

    public function getFactoryorderUuid(): string
    {
        if (!isset($this->storage['factoryorder_uuid'])) {
            return '';
        }
        return $this->storage['factoryorder_uuid'];
    }

    public function setFactoryorderUuid(string $factoryorderUuid): void
    {
        $this->storage['factoryorder_uuid'] = $factoryorderUuid;
    }

    public function getFactoryorderNo(): int
    {
        if (!isset($this->storage['factoryorder_no'])) {
            return 0;
        }
        return $this->storage['factoryorder_no'];
    }

    public function setFactoryorderNo(int $factoryorderNo): void
    {
        $this->storage['factoryorder_no'] = $factoryorderNo;
    }

    public function getProductUuid(): string
    {
        if (!isset($this->storage['product_uuid'])) {
            return '';
        }
        return $this->storage['product_uuid'];
    }

    public function setProductUuid(string $productUuid): void
    {
        $this->storage['product_uuid'] = $productUuid;
    }

    public function getOrderItemUuid(): string
    {
        if (!isset($this->storage['order_item_uuid'])) {
            return '';
        }
        return $this->storage['order_item_uuid'];
    }

    public function setOrderItemUuid(string $orderItemUuid): void
    {
        $this->storage['order_item_uuid'] = $orderItemUuid;
    }

    public function getOrderItemListUuid(): string
    {
        if (!isset($this->storage['order_item_list_uuid'])) {
            return '';
        }
        return $this->storage['order_item_list_uuid'];
    }

    public function setOrderItemListUuid(string $orderItemListUuid): void
    {
        $this->storage['order_item_list_uuid'] = $orderItemListUuid;
    }

    public function getFactoryorderBriefing(): string
    {
        if (!isset($this->storage['factoryorder_briefing'])) {
            return '';
        }
        return $this->storage['factoryorder_briefing'];
    }

    public function setFactoryorderBriefing(string $factoryorderBriefing): void
    {
        $this->storage['factoryorder_briefing'] = $factoryorderBriefing;
    }

    public function getFactoryorderQuantity(): int
    {
        if (!isset($this->storage['factoryorder_quantity'])) {
            return 0;
        }
        return $this->storage['factoryorder_quantity'];
    }

    public function setFactoryorderQuantity(int $factoryorderQuantity): void
    {
        $this->storage['factoryorder_quantity'] = $factoryorderQuantity;
    }

    public function getFactoryorderTimeCreate(): string
    {
        if (!isset($this->storage['factoryorder_time_create'])) {
            return '';
        }
        return $this->storage['factoryorder_time_create'];
    }

    public function setFactoryorderTimeCreate(string $factoryorderTimeCreate): void
    {
        $this->storage['factoryorder_time_create'] = $factoryorderTimeCreate;
    }

    public function getFactoryorderTimeUpdate(): string
    {
        if (!isset($this->storage['factoryorder_time_update'])) {
            return '';
        }
        return $this->storage['factoryorder_time_update'];
    }

    public function setFactoryorderTimeUpdate(string $factoryorderTimeUpdate): void
    {
        $this->storage['factoryorder_time_update'] = $factoryorderTimeUpdate;
    }

    public function getLocationPlaceUuid(): string
    {
        if (!isset($this->storage['location_place_uuid'])) {
            return '';
        }
        return $this->storage['location_place_uuid'];
    }

    public function getFactoryorderTimeCreateUnix(): int
    {
        if (!isset($this->storage['factoryorder_time_create_unix'])) {
            return 0;
        }
        return $this->storage['factoryorder_time_create_unix'];
    }

    public function getFactoryorderTimeFinishSchedule(): string
    {
        if (!isset($this->storage['factoryorder_time_finish_schedule'])) {
            return '';
        }
        return $this->storage['factoryorder_time_finish_schedule'];
    }

    public function getFactoryorderTimeFinishReal(): string
    {
        if (!isset($this->storage['factoryorder_time_finish_real'])) {
            return '';
        }
        return $this->storage['factoryorder_time_finish_real'];
    }

    public function getUserUuidCreate(): string
    {
        if (!isset($this->storage['user_uuid_create'])) {
            return '';
        }
        return $this->storage['user_uuid_create'];
    }

    public function setUserUuidCreate(string $userUuidCreate): void
    {
        $this->storage['user_uuid_create'] = $userUuidCreate;
    }

    public function getUserUuidUpdate(): string
    {
        if (!isset($this->storage['user_uuid_update'])) {
            return '';
        }
        return $this->storage['user_uuid_update'];
    }

    public function setUserUuidUpdate(string $userUuidUpdate): void
    {
        $this->storage['user_uuid_update'] = $userUuidUpdate;
    }

    public function getCostCentreId(): int
    {
        if (!isset($this->storage['cost_centre_id'])) {
            return 0;
        }
        return $this->storage['cost_centre_id'];
    }

    public function setCostCentreId(int $costCentreId): void
    {
        $this->storage['cost_centre_id'] = $costCentreId;
    }

    public function getQuantityunitUuid(): string
    {
        if (!isset($this->storage['quantityunit_uuid'])) {
            return '';
        }
        return $this->storage['quantityunit_uuid'];
    }

    public function getQuantityunitName(): string
    {
        if (!isset($this->storage['quantityunit_name'])) {
            return '';
        }
        return $this->storage['quantityunit_name'];
    }

    public function getQuantityunitLabel(): string
    {
        if (!isset($this->storage['quantityunit_label'])) {
            return '';
        }
        return $this->storage['quantityunit_label'];
    }

    public function getOrderNo(): int
    {
        if (!isset($this->storage['order_no'])) {
            return 0;
        }
        return $this->storage['order_no'];
    }

    public function getOrderNoCompl(): string
    {
        if (!isset($this->storage['order_no_compl'])) {
            return '';
        }
        return $this->storage['order_no_compl'];
    }

    public function getOrderTimeFinishSchedule(): string
    {
        if (!isset($this->storage['order_time_finish_schedule'])) {
            return '';
        }
        return $this->storage['order_time_finish_schedule'];
    }

    public function getLocationPlaceName(): string
    {
        if (!isset($this->storage['location_place_name'])) {
            return '';
        }
        return $this->storage['location_place_name'];
    }

    public function getLocationPlaceLabel(): string
    {
        if (!isset($this->storage['location_place_label'])) {
            return '';
        }
        return $this->storage['location_place_label'];
    }

    public function getCustomerNo(): int
    {
        if (!isset($this->storage['customer_no'])) {
            return 0;
        }
        return $this->storage['customer_no'];
    }

    public function getCustomerName(): string
    {
        if (!isset($this->storage['customer_name'])) {
            return '';
        }
        return $this->storage['customer_name'];
    }

    public function getUserDetailsNameFirst(): string
    {
        if (!isset($this->storage['user_details_name_first'])) {
            return '';
        }
        return $this->storage['user_details_name_first'];
    }

    public function getUserDetailsNameLast(): string
    {
        if (!isset($this->storage['user_details_name_last'])) {
            return '';
        }
        return $this->storage['user_details_name_last'];
    }

    public function getProductStructure(): string
    {
        if (!isset($this->storage['product_structure'])) {
            return '';
        }
        return $this->storage['product_structure'];
    }

    public function setProductStructure(string $productStructure): void
    {
        $this->storage['product_structure'] = $productStructure;
    }

    public function getProductOrigin(): string
    {
        if (!isset($this->storage['product_origin'])) {
            return '';
        }
        return $this->storage['product_origin'];
    }

    public function setProductOrigin(string $productOrigin): void
    {
        $this->storage['product_origin'] = $productOrigin;
    }

    public function getProductType(): string
    {
        if (!isset($this->storage['product_type'])) {
            return '';
        }
        return $this->storage['product_type'];
    }

    public function setProductType(string $productType): void
    {
        $this->storage['product_type'] = $productType;
    }

    public function getProductTextPart(): string
    {
        if (!isset($this->storage['product_text_part'])) {
            return '';
        }
        return $this->storage['product_text_part'];
    }

    public function setProductTextPart(string $productTextPart): void
    {
        $this->storage['product_text_part'] = $productTextPart;
    }

    public function getProductTextShort(): string
    {
        if (!isset($this->storage['product_text_short'])) {
            return '';
        }
        return $this->storage['product_text_short'];
    }

    public function setProductTextShort(string $productTextShort): void
    {
        $this->storage['product_text_short'] = $productTextShort;
    }

    public function getProductTextLong(): string
    {
        if (!isset($this->storage['product_text_long'])) {
            return '';
        }
        return $this->storage['product_text_long'];
    }

    public function setProductTextLong(string $productTextLong): void
    {
        $this->storage['product_text_long'] = $productTextLong;
    }

    public function getProductTextLicense(): string
    {
        if (!isset($this->storage['product_text_license'])) {
            return '';
        }
        return $this->storage['product_text_license'];
    }

    public function setProductTextLicense(string $productTextLicense): void
    {
        $this->storage['product_text_license'] = $productTextLicense;
    }

    public function getProductTextLicenseRemark(): string
    {
        if (!isset($this->storage['product_text_license_remark'])) {
            return '';
        }
        return $this->storage['product_text_license_remark'];
    }

    public function setProductTextLicenseRemark(string $productTextLicenseRemark): void
    {
        $this->storage['product_text_license_remark'] = $productTextLicenseRemark;
    }

    public function getProductNoNo(): int
    {
        if (!isset($this->storage['product_no_no'])) {
            return 0;
        }
        return $this->storage['product_no_no'];
    }

    public function setProductNoNo(int $productNoNo): void
    {
        $this->storage['product_no_no'] = $productNoNo;
    }

    public function getCountWorkflow(): int
    {
        if (!isset($this->storage['count_workflow'])) {
            return 0;
        }
        return $this->storage['count_workflow'];
    }

    public function setCountWorkflow(int $countWorkflow): void
    {
        $this->storage['count_workflow'] = $countWorkflow;
    }

    public function getCountLocation(): int
    {
        if (!isset($this->storage['count_location'])) {
            return 0;
        }
        return $this->storage['count_location'];
    }

    public function getSumProdQuantity(): int
    {
        if (!isset($this->storage['sum_prod_quantity'])) {
            return 0;
        }
        return $this->storage['sum_prod_quantity'];
    }

    /**
     * @return int factoryorder_quantity - sum_prod_quantity
     */
    public function getStillToBeProduced(): int
    {
        return $this->storage['factoryorder_quantity'] - $this->storage['sum_prod_quantity'];
    }

    /**
     * @param int $prodQntty Amount still to be produced.
     * @return bool TRUE if $prodQntty is OK ($prodQntty <= (factoryorder_quantity - sum_prod_quantity))
     */
    public function isValidFinishQuantity(int $prodQntty): bool
    {
        return $this->getStillToBeProduced() >= $prodQntty;
    }
}

<?php

namespace Lerp\Factoryorder\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

/**
 * Class FactoryorderWorkflowEntity
 * Data from db.view_factoryorder_workflow
 * @package Lerp\Factoryorder\Entity
 */
class FactoryorderWorkflowEntity extends AbstractEntity
{
    public array $mapping = [
        'factoryorder_workflow_uuid' => 'factoryorder_workflow_uuid',
        'factoryorder_uuid' => 'factoryorder_uuid',
        'workflow_uuid' => 'workflow_uuid',
        'product_workflow_time' => 'product_workflow_time',
        'factoryorder_workflow_time' => 'factoryorder_workflow_time',
        'factoryorder_workflow_price_per_hour' => 'factoryorder_workflow_price_per_hour',
        'factoryorder_workflow_text' => 'factoryorder_workflow_text',
        'factoryorder_workflow_order_priority' => 'factoryorder_workflow_order_priority',
        'workflow_type' => 'workflow_type',
        'workflow_label' => 'workflow_label',
        'workflow_code' => 'workflow_code',
        'workflow_is_extern' => 'workflow_is_extern',
        'workflow_price_per_hour' => 'workflow_price_per_hour',
        'factoryorder_no' => 'factoryorder_no',
        'product_uuid' => 'product_uuid',
        'order_item_uuid' => 'order_item_uuid',
        'order_item_list_uuid' => 'order_item_list_uuid',
        'factoryorder_briefing' => 'factoryorder_briefing',
        'factoryorder_quantity' => 'factoryorder_quantity',
        'factoryorder_time_create' => 'factoryorder_time_create',
        'factoryorder_time_create_unix' => 'factoryorder_time_create_unix',
    ];

    protected $primaryKey = 'factoryorder_workflow_uuid';

    public function getFactoryorderWorkflowUuid(): string
    {
        if (!isset($this->storage['factoryorder_workflow_uuid'])) {
            return '';
        }
        return $this->storage['factoryorder_workflow_uuid'];
    }

    public function setFactoryorderWorkflowUuid(string $factoryorderWorkflowUuid): void
    {
        $this->storage['factoryorder_workflow_uuid'] = $factoryorderWorkflowUuid;
    }

    public function getFactoryorderUuid(): string
    {
        if (!isset($this->storage['factoryorder_uuid'])) {
            return '';
        }
        return $this->storage['factoryorder_uuid'];
    }

    public function setFactoryorderUuid(string $factoryorderUuid): void
    {
        $this->storage['factoryorder_uuid'] = $factoryorderUuid;
    }

    public function getWorkflowUuid(): string
    {
        if (!isset($this->storage['workflow_uuid'])) {
            return '';
        }
        return $this->storage['workflow_uuid'];
    }

    public function setWorkflowUuid(string $workflowUuid): void
    {
        $this->storage['workflow_uuid'] = $workflowUuid;
    }

    public function getProductWorkflowTime(): int
    {
        if (!isset($this->storage['product_workflow_time'])) {
            return 0;
        }
        return $this->storage['product_workflow_time'];
    }

    public function setProductWorkflowTime(int $productWorkflowTime): void
    {
        $this->storage['product_workflow_time'] = $productWorkflowTime;
    }

    public function getFactoryorderWorkflowTime(): int
    {
        if (!isset($this->storage['factoryorder_workflow_time'])) {
            return 0;
        }
        return $this->storage['factoryorder_workflow_time'];
    }

    public function setFactoryorderWorkflowTime(int $factoryorderWorkflowTime): void
    {
        $this->storage['factoryorder_workflow_time'] = $factoryorderWorkflowTime;
    }

    public function getFactoryorderWorkflowPricePerHour(): float
    {
        if (!isset($this->storage['factoryorder_workflow_price_per_hour'])) {
            return '';
        }
        return $this->storage['factoryorder_workflow_price_per_hour'];
    }

    public function setFactoryorderWorkflowPricePerHour(float $factoryorderWorkflowPricePerHour): void
    {
        $this->storage['factoryorder_workflow_price_per_hour'] = $factoryorderWorkflowPricePerHour;
    }

    public function getFactoryorderWorkflowText(): string
    {
        if (!isset($this->storage['factoryorder_workflow_text'])) {
            return '';
        }
        return $this->storage['factoryorder_workflow_text'];
    }

    public function setFactoryorderWorkflowText(string $factoryorderWorkflowText): void
    {
        $this->storage['factoryorder_workflow_text'] = $factoryorderWorkflowText;
    }

    public function getFactoryorderWorkflowOrderPriority(): int
    {
        if (!isset($this->storage['factoryorder_workflow_order_priority'])) {
            return 0;
        }
        return $this->storage['factoryorder_workflow_order_priority'];
    }

    public function setFactoryorderWorkflowOrderPriority(int $factoryorderWorkflowOrderPriority): void
    {
        $this->storage['factoryorder_workflow_order_priority'] = $factoryorderWorkflowOrderPriority;
    }

    public function getWorkflowType(): string
    {
        if (!isset($this->storage['workflow_type'])) {
            return '';
        }
        return $this->storage['workflow_type'];
    }

    public function getWorkflowLabel(): string
    {
        if (!isset($this->storage['workflow_label'])) {
            return '';
        }
        return $this->storage['workflow_label'];
    }

    public function getWorkflowCode(): string
    {
        if (!isset($this->storage['workflow_code'])) {
            return '';
        }
        return $this->storage['workflow_code'];
    }

    public function getWorkflowIsExtern(): bool
    {
        if (!isset($this->storage['workflow_is_extern'])) {
            return false;
        }
        return $this->storage['workflow_is_extern'];
    }

    public function getFactoryorderNo(): int
    {
        if (!isset($this->storage['factoryorder_no'])) {
            return 0;
        }
        return $this->storage['factoryorder_no'];
    }

    public function setFactoryorderNo(int $factoryorderNo): void
    {
        $this->storage['factoryorder_no'] = $factoryorderNo;
    }

    public function getProductUuid(): string
    {
        if (!isset($this->storage['product_uuid'])) {
            return '';
        }
        return $this->storage['product_uuid'];
    }

    public function setProductUuid(string $productUuid): void
    {
        $this->storage['product_uuid'] = $productUuid;
    }

    public function getOrderItemUuid(): string
    {
        if (!isset($this->storage['order_item_uuid'])) {
            return '';
        }
        return $this->storage['order_item_uuid'];
    }

    public function setOrderItemUuid(string $orderItemUuid): void
    {
        $this->storage['order_item_uuid'] = $orderItemUuid;
    }

    public function getOrderItemListUuid(): string
    {
        if (!isset($this->storage['order_item_list_uuid'])) {
            return '';
        }
        return $this->storage['order_item_list_uuid'];
    }

    public function setOrderItemListUuid(string $orderItemListUuid): void
    {
        $this->storage['order_item_list_uuid'] = $orderItemListUuid;
    }

    public function getFactoryorderBriefing(): string
    {
        if (!isset($this->storage['factoryorder_briefing'])) {
            return '';
        }
        return $this->storage['factoryorder_briefing'];
    }

    public function setFactoryorderBriefing(string $factoryorderBriefing): void
    {
        $this->storage['factoryorder_briefing'] = $factoryorderBriefing;
    }

    public function getFactoryorderQuantity(): int
    {
        if (!isset($this->storage['factoryorder_quantity'])) {
            return 0;
        }
        return $this->storage['factoryorder_quantity'];
    }

    public function setFactoryorderQuantity(int $factoryorderQuantity): void
    {
        $this->storage['factoryorder_quantity'] = $factoryorderQuantity;
    }

    public function getFactoryorderTimeCreate(): string
    {
        if (!isset($this->storage['factoryorder_time_create'])) {
            return '';
        }
        return $this->storage['factoryorder_time_create'];
    }

    public function getFactoryorderTimeCreateUnix(): int
    {
        if (!isset($this->storage['factoryorder_time_create_unix'])) {
            return 0;
        }
        return $this->storage['factoryorder_time_create_unix'];
    }
}

<?php

namespace Lerp\Factoryorder\Entity;

class QRCodeFactoryorder
{
    protected string $productNo = '';
    protected string $factoryorderNo = '';
    protected string $orderNoCompl = '';

    /**
     * @param string $productNo
     * @param string $factoryorderNo
     * @param string $orderNoCompl
     */
    public function __construct(string $productNo, string $factoryorderNo, string $orderNoCompl)
    {
        $this->productNo = $productNo;
        $this->factoryorderNo = $factoryorderNo;
        $this->orderNoCompl = $orderNoCompl;
        return $this;
    }

    public function setProductNo(string $productNo): void
    {
        $this->productNo = $productNo;
    }

    public function setFactoryorderNo(string $factoryorderNo): void
    {
        $this->factoryorderNo = $factoryorderNo;
    }

    public function setOrderNoCompl(string $orderNoCompl): void
    {
        $this->orderNoCompl = $orderNoCompl;
    }

    public function getContent(): array
    {
        return ['p_no' => $this->productNo, 'fo_no' => $this->factoryorderNo, 'o_no' => $this->orderNoCompl];
    }

    public function getJSONContent(): string
    {
        return json_encode($this->getContent());
    }
}

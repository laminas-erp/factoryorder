<?php

namespace Lerp\Factoryorder\Entity;

use Bitkorn\Trinket\Entity\ParamsBase;
use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Laminas\Db\Sql\Select;

class ParamsFactoryorderWorkflowEquipEntity extends ParamsBase
{
    protected FilterChainStringSanitize $stringFilter;

    protected string $equipUuid;
    protected bool $alsoFinished = false;
    protected bool $onlyLinked = false;

    protected array $orderFieldsAvailable = [
        'factoryorder_no',
        'order_no',
        'customer_no',
        'customer_name',
        'location_place_label',
        'factoryorder_time_create',
        'factoryorder_time_finish_schedule',
        'factoryorder_time_finish_real',
        'product_no_no',
        'product_text_short',
        'order_item_text_short',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->stringFilter = new FilterChainStringSanitize();
    }

    public function setEquipUuid(string $equipUuid): void
    {
        $this->equipUuid = $this->uuidValid->isValid($equipUuid) ? $equipUuid : '';
        if (empty($this->equipUuid)) {
            $this->success = false;
            $this->addMessage('Equipment UUID is not valid');
        }
    }

    public function setAlsoFinished(bool $alsoFinished): void
    {
        $this->alsoFinished = $alsoFinished;
    }

    public function isAlsoFinished(): bool
    {
        return $this->alsoFinished;
    }

    public function setOnlyLinked(bool $onlyLinked): void
    {
        $this->onlyLinked = $onlyLinked;
    }

    public function isOnlyLinked(): bool
    {
        return $this->onlyLinked;
    }

    public function setFromParamsArray(array $qp): void
    {
        parent::setFromParamsArray($qp);
        $this->setEquipUuid($this->stringFilter->filter($qp['equip_uuid']) ?? '');
        $this->setAlsoFinished($qp['also_finished'] == 'true');
        $this->setOnlyLinked($qp['only_linked'] == 'true');
    }

    public function computeSelect(Select &$select, string $orderDefault = ''): void
    {
        parent::computeSelect($select);
        if (!empty($this->equipUuid)) {
            $selectEquipmentGroupUuid = new Select('equipment_group_rel');
            $selectEquipmentGroupUuid->columns(['equipment_group_uuid']);
            $selectEquipmentGroupUuid->where(['equipment_uuid' => $this->equipUuid]);
            $selectWorkflowUuid = new Select('workflow_equipment_group_rel');
            $selectWorkflowUuid->columns(['workflow_uuid']);
            $selectWorkflowUuid->where->in('equipment_group_uuid', $selectEquipmentGroupUuid);
            $select->where->in('workflow_uuid', $selectWorkflowUuid);
        }
        if (!empty($this->equipUuid) && $this->onlyLinked) {
            $select->where(['equipment_uuid' => $this->equipUuid]);
        }
        if (!$this->alsoFinished) {
            $select->where->isNull('factoryorder_time_finish_real');
        }
    }

}

<?php

namespace Lerp\Factoryorder\Entity;

use Bitkorn\Trinket\Entity\ParamsBase;
use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Filter\DateTimeFormatter;
use Laminas\Validator\Uuid;

class ParamsFactoryorderEntity extends ParamsBase
{
    protected DateTimeFormatter $dateFilter;
    protected string $dateTimeFormat = 'Y-m-d H:i:s.u';
    protected FilterChainStringSanitize $stringFilter;

    protected int $factoryorderNo;
    protected int $productNoNo;
    protected string $orderNoCompl;
    protected int $customerNo;
    protected string $customerName;
    protected string $locationPlaceUuid;
    protected string $locationPlaceLabel;
    protected string $timeCreateFrom;
    protected string $timeCreateTo;
    protected array $equipGroupUuids;

    protected array $orderFieldsAvailable = [
        'factoryorder_no',
        'order_no_compl',
        'customer_no',
        'customer_name',
        'location_place_label',
        'factoryorder_time_create',
        'factoryorder_time_finish_schedule',
        'factoryorder_time_finish_real',
        'product_no_no',
        'product_text_short',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->dateFilter = new DateTimeFormatter();
        $this->dateFilter->setFormat($this->dateTimeFormat);
        $this->stringFilter = new FilterChainStringSanitize();
    }

    public function setFactoryorderNo(int $foNo): void
    {
        $this->factoryorderNo = $foNo;
    }

    public function setProductNoNo(int $productNoNo): void
    {
        $this->productNoNo = $productNoNo;
    }

    public function setOrderNoCompl(string $orderNoCompl): void
    {
        $this->orderNoCompl = $orderNoCompl;
    }

    public function setCustomerNo(int $customerNo): void
    {
        $this->customerNo = $customerNo;
    }

    public function setCustomerName(string $customerName): void
    {
        $this->customerName = $customerName;
    }

    public function setLocationPlaceUuid(string $locationPlaceUuid): void
    {
        $this->locationPlaceUuid = $locationPlaceUuid;
    }

    public function setLocationPlaceLabel(string $locationPlaceLabel): void
    {
        $this->locationPlaceLabel = $locationPlaceLabel;
    }

    public function setTimeCreateFrom(string $timeCreateFrom): void
    {
        $time = $this->dateFilter->filter($timeCreateFrom);
        if ($time instanceof \DateTime) {
            $time = $time->format($this->dateTimeFormat);
        }
        if (!is_string($time)) {
            return;
        }
        $this->timeCreateFrom = $time;
    }

    public function setTimeCreateTo(string $timeCreateTo): void
    {
        $time = $this->dateFilter->filter($timeCreateTo);
        if ($time instanceof \DateTime) {
            $time = $time->format($this->dateTimeFormat);
        }
        if (!is_string($time)) {
            return;
        }
        $this->timeCreateTo = $time;
    }

    public function setEquipGroupUuids(array $equipGroupUuids): void
    {
        foreach ($equipGroupUuids as $equipGroupUuid) {
            if (!$this->uuidValid->isValid($equipGroupUuid)) {
                continue;
            }
            $this->equipGroupUuids[] = $equipGroupUuid;
        }
    }

    public function setFromParamsArray(array $qp): void
    {
        parent::setFromParamsArray($qp);
        $this->setFactoryorderNo(intval($qp['factoryorder_no']) ?? 0);
        $this->setProductNoNo(intval($qp['product_no_no']) ?? 0);
        $this->setOrderNoCompl($qp['order_no_compl'] ?? '');
        $this->setCustomerNo(intval($qp['customer_no']) ?? 0);
        $this->setCustomerName($this->stringFilter->filter($qp['customer_name'] ?? ''));
        $this->setLocationPlaceUuid($this->stringFilter->filter($qp['location_place_uuid']) ?? '');
        $this->setLocationPlaceLabel($this->stringFilter->filter($qp['location_place_label'] ?? ''));
        $this->setTimeCreateFrom($qp['time_create_from'] ?? '');
        $this->setTimeCreateTo($qp['time_create_to'] ?? '');
        $this->setEquipGroupUuids($qp['equipment_group_uuids'] ? explode(',', $qp['equipment_group_uuids']) : []);
    }

    public function computeSelect(Select &$select, string $orderDefault = ''): void
    {
        parent::computeSelect($select);
        if (!empty($this->factoryorderNo)) {
            $select->where->like(new Expression('CAST(factoryorder_no AS TEXT)'), '%' . $this->factoryorderNo . '%');
        }
        if (!empty($this->productNoNo)) {
            $select->where->like(new Expression('CAST(product_no_no AS TEXT)'), '%' . $this->productNoNo . '%');
        }
        if (!empty($this->orderNoCompl)) {
            $select->where->like(new Expression('order_no_compl'), '%' . $this->orderNoCompl . '%');
        }
        if (!empty($this->customerNo)) {
            $select->where->like(new Expression('CAST(customer_no AS TEXT)'), '%' . $this->customerNo . '%');
        }
        if (!empty($this->customerName)) {
            $select->where->like('customer_name', '%' . $this->customerName . '%');
        }
        if (!empty($this->locationPlaceUuid)) {
            $select->where(['location_place_uuid' => $this->locationPlaceUuid]);
        }
        if (!empty($this->locationPlaceLabel)) {
            $select->where->like('location_place_label', '%' . $this->locationPlaceLabel . '%');
        }
        if (!empty($this->timeCreateFrom)) {
            $select->where->greaterThanOrEqualTo('factoryorder_time_create', $this->timeCreateFrom);
        }
        if (!empty($this->timeCreateTo)) {
            $select->where->lessThanOrEqualTo('factoryorder_time_create', $this->timeCreateTo);
        }
        if (!empty($this->equipGroupUuids)) {
            foreach ($this->equipGroupUuids as $equipGroupUuid) {
                $equipGroupUuid = '%' . $equipGroupUuid . '%';
                $select->where->OR->like('concat_equipment_group_uuids', $equipGroupUuid);
            }
        }
    }

}

<?php

namespace Lerp\Factoryorder;

use Bitkorn\User\Entity\User\Rightsnroles;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Lerp\Factoryorder\Controller\Ajax\Equip\FactoryorderEquipAjaxController;
use Lerp\Factoryorder\Controller\Ajax\FactoryorderAjaxController;
use Lerp\Factoryorder\Controller\Ajax\OrderFactoryorderAjaxController;
use Lerp\Factoryorder\Controller\Ajax\OrderItemCreateFactoryorderAjaxController;
use Lerp\Factoryorder\Controller\Ajax\Workflow\FactoryorderWorkflowAjaxController;
use Lerp\Factoryorder\Controller\Rest\FactoryorderRestController;
use Lerp\Factoryorder\Controller\Rest\FactoryorderWorkflowRestController;
use Lerp\Factoryorder\Controller\Rest\OrderItemFactoryorderRestController;
use Lerp\Factoryorder\Controller\Rest\OrderItemListFactoryorderRestController;
use Lerp\Factoryorder\Factory\Controller\Ajax\Equip\FactoryorderEquipAjaxControllerFactory;
use Lerp\Factoryorder\Factory\Controller\Ajax\FactoryorderAjaxControllerFactory;
use Lerp\Factoryorder\Factory\Controller\Ajax\OrderFactoryorderAjaxControllerFactory;
use Lerp\Factoryorder\Factory\Controller\Ajax\OrderItemCreateFactoryorderAjaxControllerFactory;
use Lerp\Factoryorder\Factory\Controller\Ajax\Workflow\FactoryorderWorkflowAjaxControllerFactory;
use Lerp\Factoryorder\Factory\Controller\Rest\FactoryorderRestControllerFactory;
use Lerp\Factoryorder\Factory\Controller\Rest\FactoryorderWorkflowRestControllerFactory;
use Lerp\Factoryorder\Factory\Controller\Rest\OrderItemFactoryorderRestControllerFactory;
use Lerp\Factoryorder\Factory\Controller\Rest\OrderItemListFactoryorderRestControllerFactory;
use Lerp\Factoryorder\Factory\Form\FactoryorderFormFactory;
use Lerp\Factoryorder\Factory\Form\FactoryorderWorkflowFormFactory;
use Lerp\Factoryorder\Factory\Service\FactoryorderEquipServiceFactory;
use Lerp\Factoryorder\Factory\Service\FactoryorderGodServiceFactory;
use Lerp\Factoryorder\Factory\Service\FactoryorderServiceFactory;
use Lerp\Factoryorder\Factory\Service\FactoryorderWorkflowServiceFactory;
use Lerp\Factoryorder\Factory\Table\FactoryorderProdTableFactory;
use Lerp\Factoryorder\Factory\Table\FactoryorderTableFactory;
use Lerp\Factoryorder\Factory\Table\FactoryorderWorkflowTableFactory;
use Lerp\Factoryorder\Factory\Table\ViewFactoryorderWorkflowEquipTableFactory;
use Lerp\Factoryorder\Form\FactoryorderForm;
use Lerp\Factoryorder\Form\FactoryorderWorkflowForm;
use Lerp\Factoryorder\Service\FactoryorderEquipService;
use Lerp\Factoryorder\Service\FactoryorderGodService;
use Lerp\Factoryorder\Service\FactoryorderService;
use Lerp\Factoryorder\Service\FactoryorderWorkflowService;
use Lerp\Factoryorder\Table\FactoryorderProdTable;
use Lerp\Factoryorder\Table\FactoryorderTable;
use Lerp\Factoryorder\Table\FactoryorderWorkflowTable;
use Lerp\Factoryorder\Table\ViewFactoryorderWorkflowEquipTable;

return [
    'router'            => [
        'routes' => [
            /*
             * REST - order item factoryorder
             */
            'lerp_factoryorder_rest_orderitemfactoryorder'                       => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-factoryorder-rest-orderitem[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]*',
                    ],
                    'defaults'    => [
                        'controller' => OrderItemFactoryorderRestController::class,
                    ],
                ],
            ],
            /*
             * REST - order item list factoryorder
             */
            'lerp_factoryorder_rest_orderitemlistfactoryorder'                   => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-factoryorder-rest-orderitemlist[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]*',
                    ],
                    'defaults'    => [
                        'controller' => OrderItemListFactoryorderRestController::class,
                    ],
                ],
            ],
            /*
             * REST - factoryorder
             */
            'lerp_factoryorder_rest_factoryorder'                                => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-factoryorder-rest-factoryorder[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]*',
                    ],
                    'defaults'    => [
                        'controller' => FactoryorderRestController::class,
                    ],
                ],
            ],
            'lerp_factoryorderworkflow_rest_factoryorderworkflow'                => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-factoryorder-rest-factoryorderworkflow[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FactoryorderWorkflowRestController::class,
                    ],
                ],
            ],
            /*
             * AJAX
             */
            'lerp_factoryorder_ajax_workflow_factoryorderworkflow'               => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-factoryorder-ajax-update-factoryorderbriefing/:fo_uuid',
                    'constraints' => [
                        'fo_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FactoryorderWorkflowAjaxController::class,
                        'action'     => 'updateFactoryorderBriefing'
                    ],
                ],
            ],
            'lerp_factoryorder_ajax_workflow_factoryorderworkfloworderpriority'  => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-factoryorder-ajax-factoryorderworkflow-orderpriority/:fowf_uuid',
                    'constraints' => [
                        'fowf_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FactoryorderWorkflowAjaxController::class,
                        'action'     => 'updateFactoryorderWorkflowOrderPriority'
                    ],
                ],
            ],
            'lerp_factoryorder_ajax_factoryorder_bynumer'                        => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-factoryorder-ajax-by-number/:fo_no',
                    'constraints' => [
                        'fo_no' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => FactoryorderAjaxController::class,
                        'action'     => 'factoryorderByNumber'
                    ],
                ],
            ],
            'lerp_factoryorder_ajax_factoryorder_finish'                         => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-factoryorder-ajax-factoryorder-finish/:factoryorder_uuid',
                    'constraints' => [
                        'factoryorder_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FactoryorderAjaxController::class,
                        'action'     => 'finish'
                    ],
                ],
            ],
            'lerp_factoryorder_ajax_factoryorder_productstockins'                => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-factoryorder-ajax-factoryorder-product-stockins',
                    'defaults' => [
                        'controller' => FactoryorderAjaxController::class,
                        'action'     => 'productStockins'
                    ],
                ],
            ],
            'lerp_factoryorder_ajax_factoryorder_isautogenerateoperatinglicense' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-factoryorder-ajax-is-autogenerate-operating-license',
                    'defaults' => [
                        'controller' => FactoryorderAjaxController::class,
                        'action'     => 'isAutogenerateOperatingLicense'
                    ],
                ],
            ],
            /*
             * AJAX - factoryorders for order
             */
            'lerp_factoryorder_ajax_orderfactoryorder_factoryorders'             => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-factoryorder-ajax-order-factoryorders/:order_uuid',
                    'constraints' => [
                        'order_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderFactoryorderAjaxController::class,
                        'action'     => 'orderFactoryorders'
                    ],
                ],
            ],
            /*
             * AJAX - create factoryorder for order item
             */
            'lerp_factoryorder_ajax_orderitemfactoryorder_create'                => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-factoryorder-ajax-orderitem-create',
                    'defaults' => [
                        'controller' => OrderItemCreateFactoryorderAjaxController::class,
                        'action'     => 'create'
                    ],
                ],
            ],
            /*
             * AJAX - factoryorder equipment
             */
            'lerp_factoryorder_ajax_equip_factoryorder_workflows'                => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-factoryorder-ajax-equip-factoryorder-workflows',
                    'defaults' => [
                        'controller' => FactoryorderEquipAjaxController::class,
                        'action'     => 'factoryorderWorkflowsEquip'
                    ],
                ],
            ],
            'lerp_factoryorder_ajax_equip_linkfactoryordertoequip'               => [
                'type'         => Segment::class,
                'options'      => [
                    'route'       => '/lerp-factoryorder-ajax-link-factoryorder-to-equip/:fo_uuid/:equip_uuid',
                    'constraints' => [
                        'fo_uuid'    => '[0-9A-Fa-f-]+',
                        'equip_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FactoryorderEquipAjaxController::class,
                        'action'     => 'linkFactoryorderToEquip'
                    ],
                ],
                'rightsnroles' => [
                    Rightsnroles::KEY_DESC   => 'Connect a factoryorder to me',
                    Rightsnroles::KEY_GROUPS => ['project_manager'],
                ]
            ],
        ],
    ],
    'controllers'       => [
        'factories'  => [
            FactoryorderRestController::class                => FactoryorderRestControllerFactory::class,
            FactoryorderWorkflowRestController::class        => FactoryorderWorkflowRestControllerFactory::class,
            OrderItemFactoryorderRestController::class       => OrderItemFactoryorderRestControllerFactory::class,
            OrderItemListFactoryorderRestController::class   => OrderItemListFactoryorderRestControllerFactory::class,
            // AJAX
            FactoryorderWorkflowAjaxController::class        => FactoryorderWorkflowAjaxControllerFactory::class,
            FactoryorderAjaxController::class                => FactoryorderAjaxControllerFactory::class,
            OrderFactoryorderAjaxController::class           => OrderFactoryorderAjaxControllerFactory::class,
            OrderItemCreateFactoryorderAjaxController::class => OrderItemCreateFactoryorderAjaxControllerFactory::class,
            FactoryorderEquipAjaxController::class           => FactoryorderEquipAjaxControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager'   => [
        'factories'  => [
            // table
            FactoryorderTable::class                  => FactoryorderTableFactory::class,
            FactoryorderWorkflowTable::class          => FactoryorderWorkflowTableFactory::class,
            FactoryorderProdTable::class              => FactoryorderProdTableFactory::class,
            ViewFactoryorderWorkflowEquipTable::class => ViewFactoryorderWorkflowEquipTableFactory::class,
            // service
            FactoryorderGodService::class             => FactoryorderGodServiceFactory::class,
            FactoryorderService::class                => FactoryorderServiceFactory::class,
            FactoryorderWorkflowService::class        => FactoryorderWorkflowServiceFactory::class,
            FactoryorderEquipService::class           => FactoryorderEquipServiceFactory::class,
            // form
            FactoryorderForm::class                   => FactoryorderFormFactory::class,
            FactoryorderWorkflowForm::class           => FactoryorderWorkflowFormFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helpers'      => [
        'factories'  => [],
        'invokables' => [],
        'aliases'    => [],
    ],
    'view_manager'      => [
        'template_map'        => [],
        'template_path_stack' => [],
        'strategies'          => [
            'ViewJsonStrategy',
        ],
    ],
    'lerp_factoryorder' => [
        'module_brand'                    => 'factoryorder',
        'auto_generate_operating_license' => true,
    ]
];
